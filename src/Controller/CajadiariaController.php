<?php

namespace App\Controller;

use App\Entity\Cajadiaria;
use App\Form\CajadiariaType;
use App\Entity\Detacaja;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * @Route("/cajadiaria")
 */
class CajadiariaController extends AbstractController
{
    /**
     * @Route("/", name="cajadiaria_index", methods={"GET"})
     */
    public function index(): Response
    {

         date_default_timezone_set('America/Argentina/Cordoba');
         $now2 = date('Y-m-d', strtotime("+1 day"));
         $now1 = date("Y-m-d");//date_format($date,"Y/m/d H:i:s");

        // $cajadiarias = $this->getDoctrine()
        //     ->getRepository(Cajadiaria::class)
        //     ->findAll();

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder()
        ->select('c')
        ->from('App:Cajadiaria', 'c')
        ->where('c.fecha BETWEEN :firstDate AND :lastDate')
        ->setParameter('firstDate', $now1)
        ->setParameter('lastDate', $now2)
        ;

    $result = $qb->getQuery()->getResult();

    // return $result;

    $conn = $this->getDoctrine()->getManager()->getConnection();
       
            $sql = " SELECT
                tipocomp.tcmp_denomina As TIPO ,

                SUM(cajadiaria.caja_debe) As DEBE,
                SUM(cajadiaria.caja_haber)As HABER
                FROM
                cajadiaria
                INNER JOIN tipocomp ON tipocomp.id = cajadiaria.tipocomp_id
                WHERE
                cajadiaria.caja_fecha BETWEEN :desde AND :hasta 
                GROUP BY
                cajadiaria.tipocomp_id
                     
                   ";
            $stmt1 = $conn->prepare($sql);
            $params2 = array('desde' => $now1, 'hasta' => $now2);
            $stmt1->execute($params2);
            // $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

        return $this->render('cajadiaria/index.html.twig', [
            'cajadiarias' => $result,
             'unos' => $uno,
        ]);
    }


     /**
     *
     * @Route("/efecha", name="cajadiaria_efechas")
     *
     * @Method({"GET", "POST"})
     */
    public function index3(Request $request)
    {

         $now1 = $request->request->get("desde");
         $now2 = $request->request->get("hasta");
         
         date_default_timezone_set('America/Argentina/Cordoba');
         

       

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder()
        ->select('c')
        ->from('App:Cajadiaria', 'c')
        ->where('c.fecha BETWEEN :firstDate AND :lastDate')
        ->setParameter('firstDate', $now1)
        ->setParameter('lastDate', $now2)
        ;

         $result = $qb->getQuery()->getResult();

         $conn = $this->getDoctrine()->getManager()->getConnection();
       

     


            $sql = " SELECT
                tipocomp.tcmp_denomina As TIPO ,

                SUM(cajadiaria.caja_debe) As DEBE,
                SUM(cajadiaria.caja_haber)As HABER
                FROM
                cajadiaria
                INNER JOIN tipocomp ON tipocomp.id = cajadiaria.tipocomp_id
                WHERE
                cajadiaria.caja_fecha BETWEEN :desde AND :hasta 
                GROUP BY
                cajadiaria.tipocomp_id
                     
                   ";
            $stmt1 = $conn->prepare($sql);
            $params2 = array('desde' => $now1, 'hasta' => $now2);
            $stmt1->execute($params2);
            // $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();



        return $this->render('cajadiaria/index.html.twig', [
            'cajadiarias' => $result,
             'unos' => $uno,
        ]);
    }

    /**
     * @Route("/new", name="cajadiaria_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $cajadiarium = new Cajadiaria();
        $form = $this->createForm(CajadiariaType::class, $cajadiarium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cajadiarium);
            $entityManager->flush();

            return $this->redirectToRoute('cajadiaria_index');
        }

        return $this->render('cajadiaria/new.html.twig', [
            'cajadiarium' => $cajadiarium,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cajadiaria_show", methods={"GET"})
     */
    public function show(Cajadiaria $cajadiarium): Response
    {
        $detacajas = $this->getDoctrine()
            ->getRepository(Detacaja::class)
            ->findBy(array('caja' => $cajadiarium));
        return $this->render('cajadiaria/show.html.twig', [
            'cajadiarium' => $cajadiarium,
            'detacajas' => $detacajas,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="cajadiaria_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Cajadiaria $cajadiarium): Response
    {
        $form = $this->createForm(CajadiariaType::class, $cajadiarium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cajadiaria_show', array('id' => $cajadiarium->getId()));
        }

        return $this->render('cajadiaria/edit.html.twig', [
            'cajadiarium' => $cajadiarium,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cajadiaria_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Cajadiaria $cajadiarium): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cajadiarium->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cajadiarium);
            $entityManager->flush();
        }

        return $this->redirectToRoute('cajadiaria_index');
    }
}
