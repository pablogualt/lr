<?php

namespace App\Controller;


use App\Entity\Comprobante;
use App\Entity\DetacompFac;
use App\Entity\DetacompRec;
use App\Entity\Cajadiaria;
use App\Entity\Proveedor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nzo\UrlEncryptorBundle\Annotations\ParamEncryptor;
use Nzo\UrlEncryptorBundle\Annotations\ParamDecryptor;
//use WhiteOctober\TCPDFBundle\Controller\TCPDFController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Connection;#createQueryBuilder:
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\Validator\Constraints\DateTime;



class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        ////         replace this example code with whatever you need
        //        return $this->render('default/index.html.twig', [
        //            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        //        ]);
        return $this->redirectToRoute("app_login");
    }

    /**
     * @Route("/home", name="home")
     */
    public function index()
    {
        return $this->render('home/index2.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }


    /**
     * @Route("/nprove", name="nuevo_proveedor")
     *
     * @Method({"GET","POST"})
     */
    public function proveedorAction(Request $request) {

        $proveid =  $request->request->get("proveID");
        $em = $this->getDoctrine()->getManager();
       

            $status = array('id' => '');
            if ($proveid != '') {

                    $pro = new Proveedor();
                   // $caja->setPersona($idemclie);
                    $pro->setId( $proveid);
                    $em->persist($pro);
                    $em->flush();
                    $last_Id = $pro->getId();
                 $status = array('id' => $last_Id);
            }

      
         return new JsonResponse($status);

    }

   

    /**
     * Creates a new itecnico entity.
     *
     *
     * @Route("/pnew", name="p_nuevo")
     * @Method({"GET", "POST"})
     */
    public function nuevoPediwebAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idemclie = $request->request->get("idclie");
       // $idemclie =1380;
//        $idEmpl = 2;
        // $empl_id = $this->getUser()->getId();
        $ctotali = $request->request->get("ctotal");
        $compfech = $request->request->get("cfecha");
        $observ = $request->request->get("observ");
        $largo = $request->request->get("largo");
        $cventa = $request->request->get("cventa");
        // $cenvio = $request->request->get("cenvio");
        $datos = json_decode($_POST['array'], true);
        // $empleado = $em->getRepository('AppBundle:Empleado')->find( $empl_id);
        $persona = $em->getRepository('App:Persona')->find($idemclie);
        //$tipocomp = $em->getRepository('App:Tipocomp')->find(31);//compr nota de pedido
        $pedido = new Comprobante();
        date_default_timezone_set('America/Argentina/Cordoba');

        $status = array('id' => '','caja'=>0);
        //        ($idemclie != '') and
        $now = date("Y-m-d H:i:s");
        // SELECT MAX(`comp_numero`)+1 As numero FROM `comprobante` WHERE `tcmp_id`= 31

        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = "SELECT MAX(comp_numero +1) As numero FROM comprobante WHERE tcmp_id= 31
        ";
        $stmt1 = $conn->prepare($sql);
        // $params2 = array('nom' => $result, 'yeari' => intval($cinco), 'hoy' => intval($now2));
        $stmt1->execute();
        $ultimo = $stmt1->fetchAll();
        $numero = strval($ultimo[0]['numero']);
        if ($numero == '' ) {
           $numero = '1';
        }
       
        
        if(($ctotali != '')) {
            $pedido->setLogiId(1);
            $pedido->setCliente($persona);
            $pedido->setCompObservaciones($observ);
            $pedido->setCompTotal(floatval($ctotali)); //pasar de string a float valor
            $pedido->setTcmpId(31);
            $pedido->setCompFecha($compfech);
            $pedido->setCompCtacte($cventa);
            $pedido->setCompFechaVenc($compfech);
            $pedido->setCompTimestamp($now);
            $pedido->setCompNumero($numero);
            if ($cventa == 1) {
                # code...
                $pedido->setCompSaldo(floatval($ctotali));
            }else{
                $pedido->setCompSaldo(floatval(0));
            }
            
           
                    // $pedido->setReparto($cenvio);


            $em->persist($pedido);
            $em->flush();

        }
            $last_Id = $pedido->getId();
            $pediddd = $em->getRepository('App:Comprobante')->find($last_Id);
        if ($last_Id != ''){
        for ($i = 0; $i <= $largo; $i++) {

            $deta = new DetacompFac();
            $deta->setComprobante($pediddd);
            $deta->setProdId($datos[$i][0]);
            $deta->setProdCant( floatval($datos[$i][1]));
            $deta->setProdDescri($datos[$i][3]);
            $deta->setProdPvp(floatval($datos[$i][4]));
            $deta->setProdPercdto(floatval($datos[$i][5]));
            $deta->setProdImpdto(floatval($datos[$i][4] - $datos[$i][6])) ;//seria precio real menos el precio que ya tienee descuento
            $deta->setProdImpiva(floatval(0.00));
            $em->persist($deta);
            $em->flush();

            // aca seria para actualizar stock

            // $articulo = $em->find('AppBundle:Articulo',$datos[$i][0]);
            // $stockactual = $articulo->getArtiStock();
            // $articulo->setArtiStock($stockactual - floatval($datos[$i][1]));

            // $em->flush();


            }
            //cajadiaria aca 
            $last_Id2 =0;
           if ($cventa == 0) {
                if ($last_Id != ''){
                    $caja = new Cajadiaria();
                   // $caja->setPersona($idemclie);
                    $caja->setComprobante($last_Id);
                    $caja->setDescripcion('Ingreso - Nota de Pedido Nº:'.$numero);
                    $caja->setFecha($compfech);
                    $caja->setDebe(floatval($ctotali));
                    $caja->setHaber(floatval(0));
                    $tipo = $em->getRepository('App:Tipocomp')->find(31);
                    $caja->setTipoComprobante($tipo);
                    $caja->setCompTimestamp($now);
                    $em->persist($caja);
                    $em->flush();
                    $last_Id2 = $caja->getId();


                }
                
            }



            $status = array('id' => $last_Id,'caja'=>$last_Id2);
       }
        return new JsonResponse($status);
    }

    /**
     * Creates a new itecnico entity.
     *
     *
     * @Route("/pnew2", name="p_notacrecdito")
     * @Method({"GET", "POST"})
     */
    public function nuevoncreditowebAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idemclie = $request->request->get("idclie");
        // $idemclie =1380;
        //        $idEmpl = 2;
        // $empl_id = $this->getUser()->getId();
        $ctotali = $request->request->get("ctotal");
        $compfech = $request->request->get("cfecha");
        $observ = $request->request->get("observ");
        $largo = $request->request->get("largo");
        $cventa = $request->request->get("cventa");
        // $cenvio = $request->request->get("cenvio");
        $datos = json_decode($_POST['array'], true);
        // $empleado = $em->getRepository('AppBundle:Empleado')->find( $empl_id);
        $persona = $em->getRepository('App:Persona')->find($idemclie);
        //$tipocomp = $em->getRepository('App:Tipocomp')->find(31);//compr nota de pedido
        $pedido = new Comprobante();

        // $status = array('id' => '');
         $status = array('id' => '','caja'=>0);
        //        ($idemclie != '') and
        date_default_timezone_set('America/Argentina/Cordoba');
        $now = date("Y-m-d H:i:s");
        // SELECT MAX(`comp_numero`)+1 As numero FROM `comprobante` WHERE `tcmp_id`= 31

        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = "SELECT MAX(comp_numero +1) As numero FROM comprobante WHERE tcmp_id= 33
        ";
        $stmt1 = $conn->prepare($sql);
        
        // $params2 = array('nom' => $result, 'yeari' => intval($cinco), 'hoy' => intval($now2));
        $stmt1->execute();
        $ultimo = $stmt1->fetchAll();
        $numero = strval($ultimo[0]['numero']);
        if ($numero == '' ) {
           $numero = '1';
        }
        if (($ctotali != '')) {
            $pedido->setLogiId(1);
            $pedido->setCliente($persona);
            $pedido->setCompObservaciones($observ);
            $pedido->setCompTotal(floatval($ctotali)); //pasar de string a float valor
            $pedido->setTcmpId(33);
            $pedido->setCompFecha($compfech);
            $pedido->setCompCtacte($cventa);
            $pedido->setCompFechaVenc($compfech);
            $pedido->setCompTimestamp($now);
            $pedido->setCompNumero($numero);
            // $pedido->setReparto($cenvio);
            if ($cventa == 1) {
                # code...
                $pedido->setCompSaldo(floatval($ctotali));
            }else{
                $pedido->setCompSaldo(floatval(0));
            }

            $em->persist($pedido);
            $em->flush();
        }
        $last_Id = $pedido->getId();
        $pediddd = $em->getRepository('App:Comprobante')->find($last_Id);
        if ($last_Id != '') {
            for ($i = 0; $i <= $largo; $i++) {

                $deta = new DetacompFac();
                $deta->setComprobante($pediddd);
                $deta->setProdId($datos[$i][0]);
                $deta->setProdCant(floatval($datos[$i][1]));
                $deta->setProdDescri($datos[$i][3]);
                $deta->setProdPvp(floatval($datos[$i][4]));
                $deta->setProdPercdto(floatval($datos[$i][5]));
                $deta->setProdImpdto(floatval($datos[$i][4] - $datos[$i][6])); //seria precio real menos el precio que ya tienee descuento
                $deta->setProdImpiva(floatval(0.00));
                $em->persist($deta);
                $em->flush();

                // aca seria para actualizar stock

                // $articulo = $em->find('AppBundle:Articulo',$datos[$i][0]);
                // $stockactual = $articulo->getArtiStock();
                // $articulo->setArtiStock($stockactual - floatval($datos[$i][1]));

                // $em->flush();


            }
 //cajadiaria aca 
            $last_Id2 =0;
           if ($cventa == 0) {
                if ($last_Id != ''){
                    $caja = new Cajadiaria();
                   // $caja->setPersona($idemclie);
                    $caja->setComprobante($last_Id);
                    $caja->setDescripcion('Egreso- Nota de Credito Nº:'.$numero);
                    $caja->setFecha($compfech);
                    $caja->setDebe(floatval(0));
                    $caja->setHaber(floatval($ctotali));
                    $tipo = $em->getRepository('App:Tipocomp')->find(33);
                    $caja->setTipoComprobante($tipo);
                    $caja->setCompTimestamp($now);
                    $em->persist($caja);
                    $em->flush();
                    $last_Id2 = $caja->getId();


                }
                
            }



            $status = array('id' => $last_Id,'caja'=>$last_Id2);


            // $status = array('id' => $last_Id);
        }
        return new JsonResponse($status);
    }

    /**
     * Recibo 
     *
     *
     * @Route("/pnew3", name="p_recibo")
     * @Method({"GET", "POST"})
     */
    public function nuevorecibowebAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idemclie = $request->request->get("idclie");
        // $idemclie =1380;
        //        $idEmpl = 2;
        // $empl_id = $this->getUser()->getId();
        $ctotali = $request->request->get("ctotal");
        $compfech = $request->request->get("cfecha");
        $observ = $request->request->get("observ");
        //$largo = $request->request->get("largo");
        $cventa = $request->request->get("cventa");
        $concept = $request->request->get("concept");
        // $cenvio = $request->request->get("cenvio");
       // $datos = json_decode($_POST['array'], true);
        // $empleado = $em->getRepository('AppBundle:Empleado')->find( $empl_id);
        $persona = $em->getRepository('App:Persona')->find($idemclie);
        //$tipocomp = $em->getRepository('App:Tipocomp')->find(31);//compr nota de pedido
        $pedido = new Comprobante();

        $status = array('id' => '','caja'=>0);
        //        ($idemclie != '') and
        date_default_timezone_set('America/Argentina/Cordoba');
        $now = date("Y-m-d H:i:s");
        // SELECT MAX(`comp_numero`)+1 As numero FROM `comprobante` WHERE `tcmp_id`= 31

        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = "SELECT MAX(comp_numero)+1 As numero FROM comprobante WHERE tcmp_id= 32 "; //31 np edido 32 recibo 33 nota de credito de pedidos
        $stmt1 = $conn->prepare($sql);
       
        $stmt1->execute();
        $ultimo = $stmt1->fetchAll();

        $numero = strval($ultimo[0]['numero']);
        if ($numero == '' ) {
           $numero = '1';
        }
        if (($ctotali != '')) {
            $pedido->setLogiId(1);
            $pedido->setCliente($persona);
            $pedido->setCompObservaciones($observ);
            $pedido->setCompTotal(floatval($ctotali)); //pasar de string a float valor
            $pedido->setTcmpId(32);
            $pedido->setCompFecha($compfech);
            $pedido->setCompCtacte($cventa);
            $pedido->setCompFechaVenc($compfech);
            $pedido->setCompTimestamp($now);
            $pedido->setCompNumero($numero);
            // $pedido->setReparto($cenvio);
            if ($cventa == 1) {
                # code...
                $pedido->setCompSaldo(floatval($ctotali));
            }else{
                $pedido->setCompSaldo(floatval(0));
            }

            $em->persist($pedido);
            $em->flush();
        }
        $last_Id = $pedido->getId();
        $pediddd = $em->getRepository('App:Comprobante')->find($last_Id);
        if ($last_Id != '') {
            // for ($i = 0; $i <= $largo; $i++) {

                $deta = new DetacompRec();
                $deta->setComprobante($pediddd);
                $deta->setDecoConcepto($concept);
                $deta->setCbtasocId(intval(0)); //comprobante asosciado si hubiera
                $deta->setImporte(floatval($ctotali));
                $deta->setDecoObse($observ);
                // $deta->setProdCant(floatval($datos[$i][1]));
                // $deta->setProdDescri($datos[$i][3]);
                // $deta->setProdPvp(floatval($datos[$i][4]));
                // $deta->setProdPercdto(floatval($datos[$i][5]));
                // $deta->setProdImpdto(floatval($datos[$i][4] - $datos[$i][6])); //seria precio real menos el precio que ya tienee descuento
                // $deta->setProdImpiva(floatval(0.00));
                $em->persist($deta);
                $em->flush();

                // aca seria para actualizar stock

                // $articulo = $em->find('AppBundle:Articulo',$datos[$i][0]);
                // $stockactual = $articulo->getArtiStock();
                // $articulo->setArtiStock($stockactual - floatval($datos[$i][1]));

                // $em->flush();


            // }

            $last_Id2 =0;
           // if ($cventa == 0) {
                if ($last_Id != ''){
                    $caja = new Cajadiaria();
                   // $caja->setPersona($idemclie);
                    $caja->setComprobante($last_Id);
                    $caja->setDescripcion('Ingreso- Recibo Nº:'.$numero);
                    $caja->setFecha($compfech);
                    $caja->setDebe(floatval($ctotali));
                    $caja->setHaber(floatval(0));
                    $tipo = $em->getRepository('App:Tipocomp')->find(32);
                    $caja->setTipoComprobante($tipo);
                    $caja->setCompTimestamp($now);
                    $em->persist($caja);
                    $em->flush();
                    $last_Id2 = $caja->getId();


                }
                
            // }



            $status = array('id' => $last_Id,'caja'=>$last_Id2);


            //$status = array('id' => $last_Id);
        }
        return new JsonResponse($status);
    }

    /**
     * Recibo 
     *
     *
     * @Route("/pnew4", name="p_recibo02")
     * @Method({"GET", "POST"})
     */
    public function nuevorecibo2webAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idemclie = $request->request->get("idclie");
        // $idemclie =1380;
        //        $idEmpl = 2;
        // $empl_id = $this->getUser()->getId();
        $ctotali = $request->request->get("ctotal");
        $compfech = $request->request->get("cfecha");
        $observ = $request->request->get("observ");
        $largo = $request->request->get("largo");
        $cventa = $request->request->get("cventa");
        $concept = $request->request->get("concept");
        // $cenvio = $request->request->get("cenvio");
        $datos = json_decode($_POST['array'], true);
        // $empleado = $em->getRepository('AppBundle:Empleado')->find( $empl_id);
        $persona = $em->getRepository('App:Persona')->find($idemclie);
        //$tipocomp = $em->getRepository('App:Tipocomp')->find(31);//compr nota de pedido
        $pedido = new Comprobante();
        $deta = new DetacompRec();
        $mensajes = ' Comprobantes Asoc:';
        $status = array('id' => '','caja'=>0);
         $last_Id2 =0;
        date_default_timezone_set('America/Argentina/Cordoba');
        //        ($idemclie != '') and
        $now = date("Y-m-d H:i:s");
        // SELECT MAX(`comp_numero`)+1 As numero FROM `comprobante` WHERE `tcmp_id`= 31

        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = "SELECT MAX(comp_numero +1) As numero FROM comprobante WHERE tcmp_id= 32 "; //31 np edido 32 recibo 33 nota de credito de pedidos
        $stmt1 = $conn->prepare($sql);
        $stmt1->execute();
        $ultimo = $stmt1->fetchAll();

        $numero = strval($ultimo[0]['numero']);
        if ($numero == '' ) {
           $numero = '1';
        }

       
            ///---------------- totales a cancelar 
        $tot_a_cancel = 0.00;
        $saldo =0.00;
        // $saldo = $ctotali - $tot_a_cancel;//ver aca porque si es negativo el total eje recibo ynota de creditos no apropiadas 

        for ($i = 0; $i <= $largo; $i++) {

              $tot_a_cancel += floatval($datos[$i][5]);
                // $deta->setImportehitorico(floatval($datos[$i][3])); 
        }
        $accion = 0;// 1 apropiado 2 sobra saldo 
        if ($ctotali < 0) 
        {
             $last_Id2 =0;
         //la persona tiene plata a favor de ditintos recibos y nota de credito
           $saldo = $ctotali * (-1);
           //----------------------
            $pedido->setLogiId(1);
            $pedido->setCliente($persona);
            $pedido->setCompObservaciones($observ);
            
            $pedido->setTcmpId(32);
            $pedido->setCompFecha($compfech);
            $pedido->setCompCtacte($cventa);
            $pedido->setCompFechaVenc($compfech);
            $pedido->setCompTimestamp($now);
            $pedido->setCompNumero($numero);
            $pedido->setCompTotal(floatval($saldo)); 
            $pedido->setCompSaldo(floatval($saldo));
            $pedido->setCompApro(0);
            $em->persist($pedido);
            $em->flush();
            $last_Id = $pedido->getId();
            $pediddd = $em->getRepository('App:Comprobante')->find($last_Id);
            if ($last_Id != '') {

                
                $deta->setComprobante($pediddd);
                $deta->setDecoConcepto($concept);
                $deta->setCbtasocId(intval(0)); //comprobante asosciado si hubiera
                $deta->setImporte(floatval($ctotali));
                $deta->setDecoObse($observ);
              
                $em->persist($deta);
                $em->flush();

                for ($i = 0; $i <= $largo; $i++) {
                    // $compro = $em->getRepository('App:Comprobante')->find($datos[$i][0]);
                    // $saldo2 = 0.00;
                    $qb = $em->createQueryBuilder();
                    $q = $qb->update('App:Comprobante', 'c')
                            ->set('c.compApro', '?1')
                            // ->set('c.compSaldo', '?2')
                            ->where('c.id = ?2')
                            ->setParameter(1, 1)
                            // ->setParameter(2, 0)
                            ->setParameter(2, $datos[$i][0])
                            ->getQuery();
                    $p = $q->execute();
                    $mensajes =$mensajes. $datos[$i][1].' - '.$datos[$i][2].' , ' ;

                    }
                    $qb2 = $em->createQueryBuilder();
                    $q2 = $qb2->update('App:Comprobante', 'c')
                            ->set('c.compObservaciones', '?1')
                            // ->set('c.compSaldo', '?2')
                            ->where('c.id = ?2')
                            ->setParameter(1, $observ . ' '.$mensajes)
                            // ->setParameter(2, 0)
                            ->setParameter(2, $last_Id)
                            ->getQuery();
                    $p2 = $q2->execute();
                }
        }elseif ($ctotali >= 0) 
        {
            $resto = number_format( $ctotali - $tot_a_cancel,1 ); //aca lo restamos y lo pasomos a un decimal al resultado para comprobar
            ///-----------------
             if ($resto == 0.0) 
             {//apropiado saldo cero
                $saldo = 0 ;
                //----------------------
            $pedido->setLogiId(1);
            $pedido->setCliente($persona);
            $pedido->setCompObservaciones($observ);
            
            $pedido->setTcmpId(32);
            $pedido->setCompFecha($compfech);
            $pedido->setCompCtacte($cventa);
            $pedido->setCompFechaVenc($compfech);
            $pedido->setCompTimestamp($now);
            $pedido->setCompNumero($numero);
            $pedido->setCompTotal(floatval($ctotali)); 
            $pedido->setCompSaldo(floatval($saldo));
            $pedido->setCompApro(1);
            $em->persist($pedido);
            $em->flush();
            $last_Id = $pedido->getId();
            $pediddd = $em->getRepository('App:Comprobante')->find($last_Id);
            if ($last_Id != '') {

                
                $deta->setComprobante($pediddd);
                $deta->setDecoConcepto($concept);
                $deta->setCbtasocId(intval(0)); //comprobante asosciado si hubiera
                $deta->setImporte(floatval($ctotali));
                $deta->setDecoObse($observ);
              
                $em->persist($deta);
                $em->flush();

                for ($i = 0; $i <= $largo; $i++) {
                    // $compro = $em->getRepository('App:Comprobante')->find($datos[$i][0]);
                    // $saldo2 = 0.00;
                    $qb = $em->createQueryBuilder();
                    $q = $qb->update('App:Comprobante', 'c')
                            ->set('c.compApro', '?1')
                            // ->set('c.compSaldo', '?2')
                            ->where('c.id = ?2')
                            ->setParameter(1, 1)
                            // ->setParameter(2, 0)
                            ->setParameter(2, $datos[$i][0])
                            ->getQuery();
                    $p = $q->execute();
                    $mensajes =$mensajes. $datos[$i][1].' - '.$datos[$i][2].' , ' ;

                    }
                    $qb2 = $em->createQueryBuilder();
                    $q2 = $qb2->update('App:Comprobante', 'c')
                            ->set('c.compObservaciones', '?1')
                            // ->set('c.compSaldo', '?2')
                            ->where('c.id = ?2')
                            ->setParameter(1, $observ . ' '.$mensajes)
                            // ->setParameter(2, 0)
                            ->setParameter(2, $last_Id)
                            ->getQuery();
                    $p2 = $q2->execute();


                    $last_Id2 =0;
           // if ($cventa == 0) { //aca tnfdria que pasar a la caja cuando sea mayor que los documentos saldados 
                if ($last_Id != ''){
                    $caja = new Cajadiaria();
                   // $caja->setPersona($idemclie);
                    $caja->setComprobante($last_Id);
                    $caja->setDescripcion('Ingreso- Recibo Nº:'.$numero);
                    $caja->setFecha($compfech);
                    $caja->setDebe(floatval($ctotali));
                    $caja->setHaber(floatval(0));
                    $tipo = $em->getRepository('App:Tipocomp')->find(32);
                    $caja->setTipoComprobante($tipo);
                    $caja->setCompTimestamp($now);
                    $em->persist($caja);
                    $em->flush();
                    $last_Id2 = $caja->getId();


                     }
                
            // }

               }

            }elseif ($resto >= 0.0) {//entrega mas que recibo saldo a favor cliente no queda a propiado ese recibo $ctotali > $tot_a_cancel
               
                $saldo = $ctotali - $tot_a_cancel;//saldo a favor cliente

                    //----------------------
            $pedido->setLogiId(1);
            $pedido->setCliente($persona);
            $pedido->setCompObservaciones($observ);
            
            $pedido->setTcmpId(32);
            $pedido->setCompFecha($compfech);
            $pedido->setCompCtacte($cventa);
            $pedido->setCompFechaVenc($compfech);
            $pedido->setCompTimestamp($now);
            $pedido->setCompNumero($numero);
            $pedido->setCompTotal(floatval($tot_a_cancel)); 
            $pedido->setCompSaldo(floatval($saldo));
            $pedido->setCompApro(0);
            $em->persist($pedido);
            $em->flush();
            $last_Id = $pedido->getId();
            $pediddd = $em->getRepository('App:Comprobante')->find($last_Id);
            if ($last_Id != '') {

                
                $deta->setComprobante($pediddd);
                $deta->setDecoConcepto($concept);
                $deta->setCbtasocId(intval(0)); //comprobante asosciado si hubiera
                $deta->setImporte(floatval($ctotali));
                $deta->setDecoObse($observ);
              
                $em->persist($deta);
                $em->flush();



                for ($i = 0; $i <= $largo; $i++) {
                 
                    $qb = $em->createQueryBuilder();
                    $q = $qb->update('App:Comprobante', 'c')
                            ->set('c.compApro', '?1')
                            // ->set('c.compSaldo', '?2')
                            ->where('c.id = ?2')
                            ->setParameter(1, 1)
                            // ->setParameter(2, 0)
                            ->setParameter(2, $datos[$i][0])
                            ->getQuery();
                    $p = $q->execute();
                    $mensajes =$mensajes. $datos[$i][1].' - '.$datos[$i][2].' , ' ;

                    }
                    $qb2 = $em->createQueryBuilder();
                    $q2 = $qb2->update('App:Comprobante', 'c')
                            ->set('c.compObservaciones', '?1')
                            // ->set('c.compSaldo', '?2')
                            ->where('c.id = ?2')
                            ->setParameter(1, $observ . ' '.$mensajes)
                            // ->setParameter(2, 0)
                            ->setParameter(2, $last_Id)
                            ->getQuery();
                    $p2 = $q2->execute();
                }

            }elseif ($resto < 0.0 ) {//entrega menos que lo seleccionado  algunos documentos no quedarian saldados $ctotali < $tot_a_cancel
               
                $saldo = $ctotali ;  /// resto compronbante y si queda saldo 0 lo apropio sino pongo saldo y no loapropio 

                    //----------------------
                $pedido->setLogiId(1);
                $pedido->setCliente($persona);
                $pedido->setCompObservaciones($observ);
                
                $pedido->setTcmpId(32);
                $pedido->setCompFecha($compfech);
                $pedido->setCompCtacte($cventa);
                $pedido->setCompFechaVenc($compfech);
                $pedido->setCompTimestamp($now);
                $pedido->setCompNumero($numero);
                $pedido->setCompTotal(floatval($ctotali)); 
                $pedido->setCompSaldo(floatval($ctotali)); //floatval(0));
                $pedido->setCompApro(0);//como no llega asaldar lo que quiere pagar se hace un recibo normal y no se apropia este recibo va quedar con saldo 
                $em->persist($pedido);
                $em->flush();
                $last_Id = $pedido->getId();
                $pediddd = $em->getRepository('App:Comprobante')->find($last_Id);
                if ($last_Id != '') {

                    
                    $deta->setComprobante($pediddd);
                    $deta->setDecoConcepto($concept);
                    $deta->setCbtasocId(intval(0)); //comprobante asosciado si hubiera
                    $deta->setImporte(floatval($ctotali));
                    $deta->setDecoObse($observ);
                  
                    $em->persist($deta);
                    $em->flush();

               // $apro = 0; 
               // $resto = 0.00;

                // for ($i = 0; $i <= $largo; $i++) {

                //     $totalcompro =  floatval($datos[$i][5]);
                //     if ($totalcompro < 0) {
                //      $saldo =  $saldo - ($totalcompro * (-1));
                //       $resto = 0;
                //        $apro = 1; 

                //     } elseif ($totalcompro > 0) {
                //         if ($saldo >= $totalcompro) {
                //         $saldo = $saldo -  $totalcompro ;
                //          $resto = 0;
                //          $apro = 1; 
                //         }elseif ($saldo < $totalcompro) {
                //         $saldo =  $saldo -  $totalcompro ;
                //         $resto = $saldo*(-1);
                //          $apro = 0; 
                //         }
                      
                //     }
                 
                //     $qb = $em->createQueryBuilder();
                //     $q = $qb->update('App:Comprobante', 'c')
                //             ->set('c.compApro', '?1')
                //              ->set('c.compSaldo', '?2')
                //             ->where('c.id = ?3')
                //             ->setParameter(1, $apro)
                //              ->setParameter(2, $resto)
                //             ->setParameter(3, $datos[$i][0])
                //             ->getQuery();
                //     $p = $q->execute();
                //     $mensajes =$mensajes. $datos[$i][1].' - '.$datos[$i][2].' , ' ;

                //     }
                //     $qb2 = $em->createQueryBuilder();
                //     $q2 = $qb2->update('App:Comprobante', 'c')
                //             ->set('c.compObservaciones', '?1')
                //             // ->set('c.compSaldo', '?2')
                //             ->where('c.id = ?2')
                //             ->setParameter(1, $observ . ' '.$mensajes)
                //             // ->setParameter(2, 0)
                //             ->setParameter(2, $last_Id)
                //             ->getQuery();
                //     $p2 = $q2->execute();


                $last_Id2 =0;
           // if ($cventa == 0) { //aca tnfdria que pasar a la caja cuando sea mayor que los documentos saldados 
                if ($last_Id != ''){
                    $caja = new Cajadiaria();
                   // $caja->setPersona($idemclie);
                    $caja->setComprobante($last_Id);
                    $caja->setDescripcion('Ingreso- Recibo Nº:'.$numero);
                    $caja->setFecha($compfech);
                    $caja->setDebe(floatval($ctotali));
                    $caja->setHaber(floatval(0));
                    $tipo = $em->getRepository('App:Tipocomp')->find(32);
                    $caja->setTipoComprobante($tipo);
                    $caja->setCompTimestamp($now);
                    $em->persist($caja);
                    $em->flush();
                    $last_Id2 = $caja->getId();


                }
                
            // }


            }
           
        }
            sleep(3);

            $status = array('id' => $last_Id,'caja'=>$last_Id2);
     
            
        }
        return new JsonResponse($status);
    }

    /**
     * @Route("/nroleg", name="nroleg", methods={"GET"})
     *
     */
    public function nrolegAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        //       $provincia = $em->getRepository('AppBundle:Provincia')->find($provid);
        // $localidad= $em->getRepository('AppBundle:Persona')->findByProv($provincia);

        // $pin1 = $cliente->getPin();
        $query = $em->createQuery(
            'SELECT Max(p.nrolegajo +1)  FROM App:Persona p
                '
        );

        $nro = $query->getArrayResult();

        return new JsonResponse($nro);
    }

    

    /**
     * @Route("/impnp/{id}", name="pdfnp", methods={"GET"})
     * 
     */
    public function indexAction2(Request $request,$id)
    {
// @ParamDecryptor(params={"id"})
        // $idem = $this->get('nzo_url_encryptor')->decrypt($id);
        // $em = $this->getDoctrine()->getManager('customer');
        // $comprobante = $em->getRepository('App:Comprobanteweb')->find($id);

        // $deta = $em->getRepository('App:Detacompweb')->findBy(array('comprobante'=>$id));
       $em = $this->getDoctrine()->getManager();
        //$deleteForm = $this->createDeleteForm($pedidosweb);
        $comprobantes = $this->getDoctrine()
            ->getRepository(Comprobante::class)
            ->find($id);
       // $pedidoswebs = $em->getRepository('AppBundle:Pedidosweb')->find($id);
        //$detapedidoswebs = $em->getRepository('App:DetacompFac')->findBy(array('comprobante' => $comprobantes));
        $detacompFacs = $this->getDoctrine()
            ->getRepository(DetacompFac::class)
            ->findBy(array('comprobante' => $comprobantes));
           
// return $this->render('comprobante/show.html.twig', array(
//             'comprobante' => $comprobantes,

//             'detacomp_facs' => $detacompFacs,
//             //'delete_form' => $deleteForm->createView(),
//         ));

        $html = $this->renderView('Impresion/pdfnp.twig',array(
                'comprobante' => $comprobantes,

            'detacomp_facs' => $detacompFacs,
            ));

        $this->returnPDFAutomotor($html  );
    }

    public function returnPDFAutomotor($html)
    {
        $pdf = new \jonasarts\Bundle\TCPDFBundle\TCPDF\TCPDF('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
       // create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetCreator('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
    $pdf->SetAuthor('Presupuesto');
        $pdf->SetTitle('Presupuesto');//'Orden de Servicio Tecnico'
        $pdf->SetSubject('');
        $pdf->setFontSubsetting(true);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
     $style = array(
            'position' => '',
            'align' => 'L',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => 'L',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        $style2 = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => 'C',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0);
        $pdf->SetDrawColor(1, 0, 0);
        $pdf->SetLineWidth(0.3);
//    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.'Prueba', PDF_HEADER_STRING);
        $pdf->SetFont('helvetica', '', 9, '', true);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        // $pdf->AddPage();
        $pdf->AddPage('L', 'A5');
        // set text shadow effect
        // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


         $pdf->Line(5, 140, 205, 140, $style);
        //        $pdf->Cell(0, 0, 'Orden de Servicio Tecnico', 0, 1);
         $pdf->Cell(0, 0, 'Original', 1, 1, 'C');
         $pdf->Ln(2);
        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');
         $pdf->Ln(8);
         $pdf->AddPage('L', 'A5');
          // $pdf->Line(5,5);
           // $pdf->Line(0, 5, 230, 5, $style);
         $pdf->Cell(0, 0, 'Duplicado', 1, 1, 'C');
        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');
        // $pdf->write1DBarcode($codigointerno, 'C39', '', '', '', 18, 0.4, $style2, 'R');//genera codigo de barra
        $pdf->Line(5, 140, 205, 140, $style);
        //    $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        // write some JavaScript code
        // $js = <<<EOD
        // app.alert('JavaScript Popup Example', 3, 0, 'Welcome');
        // var cResponse = app.response({
        //     cQuestion: 'How are you today?',
        //     cTitle: 'Your Health Status',
        //     cDefault: 'Fine',
        //     cLabel: 'Response:'
        // });
        // if (cResponse == null) {
        //     app.alert('Thanks for trying anyway.', 3, 0, 'Result');
        // } else {
        //     app.alert('You responded, "'+cResponse+'", to the health question.', 3, 0, 'Result');
        // }
        // EOD;

        // force print dialog
        // $js .= 'print(true);';

        // // set javascript
      $pdf->IncludeJS('print(true);');




        $hora = date('d-m-Y_H-i-s');
        $filename = 'Presupuesto_'.$hora;
        $pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
    }



/**
     * @Route("/imprec/{id}", name="pdfrec", methods={"GET"})
     * 
     */
    public function indexPrintrec(Request $request,$id)
    {
// @ParamDecryptor(params={"id"})
        // $idem = $this->get('nzo_url_encryptor')->decrypt($id);
        // $em = $this->getDoctrine()->getManager('customer');
        // $comprobante = $em->getRepository('App:Comprobanteweb')->find($id);

        // $deta = $em->getRepository('App:Detacompweb')->findBy(array('comprobante'=>$id));
       $em = $this->getDoctrine()->getManager();
        //$deleteForm = $this->createDeleteForm($pedidosweb);
        $comprobantes = $this->getDoctrine()
            ->getRepository(Comprobante::class)
            ->find($id);
       // $pedidoswebs = $em->getRepository('AppBundle:Pedidosweb')->find($id);
        //$detapedidoswebs = $em->getRepository('App:DetacompFac')->findBy(array('comprobante' => $comprobantes));
        $DetacompRecs = $this->getDoctrine()
            ->getRepository(DetacompRec::class)
            ->findBy(array('comprobante' => $comprobantes));
           
// return $this->render('comprobante/show.html.twig', array(
//             'comprobante' => $comprobantes,

//             'detacomp_facs' => $detacompFacs,
//             //'delete_form' => $deleteForm->createView(),
//         ));

        $html = $this->renderView('Impresion/pdfrecibo.twig',array(
                'comprobante' => $comprobantes,

            'detacomp_recs' => $DetacompRecs,
            ));

        $this->returnPDFRecibo($html  );
    }

    public function returnPDFRecibo($html)
    {
        $pdf = new \jonasarts\Bundle\TCPDFBundle\TCPDF\TCPDF('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
       // create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetCreator('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
    $pdf->SetAuthor('Presupuesto');
        $pdf->SetTitle('Presupuesto');//'Orden de Servicio Tecnico'
        $pdf->SetSubject('');
        $pdf->setFontSubsetting(true);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
     $style = array(
            'position' => '',
            'align' => 'L',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => 'L',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        $style2 = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => 'C',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0);
        $pdf->SetDrawColor(1, 0, 0);
        $pdf->SetLineWidth(0.3);
//    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.'Prueba', PDF_HEADER_STRING);
        $pdf->SetFont('helvetica', '', 9, '', true);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        // $pdf->AddPage();
        $pdf->AddPage('L', 'A5');
        // set text shadow effect
        // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


         $pdf->Line(5, 140, 205, 140, $style);
        //        $pdf->Cell(0, 0, 'Orden de Servicio Tecnico', 0, 1);
         $pdf->Cell(0, 0, 'Original', 1, 1, 'C');
         $pdf->Ln(2);
        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');
         $pdf->Ln(8);
         $pdf->AddPage('L', 'A5');
          // $pdf->Line(5,5);
           // $pdf->Line(0, 5, 230, 5, $style);
         $pdf->Cell(0, 0, 'Duplicado', 1, 1, 'C');
        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');
        // $pdf->write1DBarcode($codigointerno, 'C39', '', '', '', 18, 0.4, $style2, 'R');//genera codigo de barra
        $pdf->Line(5, 140, 205, 140, $style);
        //    $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        // write some JavaScript code
        // $js = <<<EOD
        // app.alert('JavaScript Popup Example', 3, 0, 'Welcome');
        // var cResponse = app.response({
        //     cQuestion: 'How are you today?',
        //     cTitle: 'Your Health Status',
        //     cDefault: 'Fine',
        //     cLabel: 'Response:'
        // });
        // if (cResponse == null) {
        //     app.alert('Thanks for trying anyway.', 3, 0, 'Result');
        // } else {
        //     app.alert('You responded, "'+cResponse+'", to the health question.', 3, 0, 'Result');
        // }
        // EOD;

        // force print dialog
        // $js .= 'print(true);';

        // // set javascript
         $pdf->IncludeJS('print(true);');

        date_default_timezone_set('America/Argentina/Cordoba');


        $hora = date('d-m-Y_H-i-s');
        $filename = 'Recibo_'.$hora;
        $pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
    }

    /**
     * Recibo 
     *
     *
     * @Route("/pin", name="p_ingreso")
     * @Method({"GET", "POST"})
     */
    public function nuevoingresobAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idemclie = $request->request->get("idclie");
        // $idemclie =1380;
        //        $idEmpl = 2;
        // $empl_id = $this->getUser()->getId();
        $ctotali = $request->request->get("ctotal");
        $compfech = $request->request->get("cfecha");
        $observ = $request->request->get("observ");
        //$largo = $request->request->get("largo");
        $cventa = $request->request->get("cventa");
        $concept = $request->request->get("concept");
        // $cenvio = $request->request->get("cenvio");
       // $datos = json_decode($_POST['array'], true);
        // $empleado = $em->getRepository('AppBundle:Empleado')->find( $empl_id);
        $persona = $em->getRepository('App:Persona')->find($idemclie);
        //$tipocomp = $em->getRepository('App:Tipocomp')->find(31);//compr nota de pedido
        $pedido = new Comprobante();

        $status = array('id' => '','caja'=>0);
        //        ($idemclie != '') and
        date_default_timezone_set('America/Argentina/Cordoba');
        $now = date("Y-m-d H:i:s");
        // SELECT MAX(`comp_numero`)+1 As numero FROM `comprobante` WHERE `tcmp_id`= 31

        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = "SELECT MAX(comp_numero)+1 As numero FROM comprobante WHERE tcmp_id= :tdoc "; //31 np edido 32 recibo 33 nota de credito de pedidos
        $stmt1 = $conn->prepare($sql);
        $params2 = array('tdoc' => $cventa);
        $stmt1->execute($params2);
        $stmt1->execute();

        $ultimo = $stmt1->fetchAll();

        $numero = strval($ultimo[0]['numero']);
        if ($numero == '' ) {
           $numero = '1';
        }
        if (($ctotali != '')) {
            $pedido->setLogiId(1);
            $pedido->setCliente($persona);
            $pedido->setCompObservaciones($concept);
            $pedido->setCompTotal(floatval($ctotali)); //pasar de string a float valor
            $pedido->setTcmpId($cventa);
            $pedido->setCompFecha($compfech);
            $pedido->setCompCtacte(0);
            $pedido->setCompFechaVenc($compfech);
            $pedido->setCompTimestamp($now);
            $pedido->setCompNumero($numero);
            // $pedido->setReparto($cenvio);
            if ($cventa == 1) {
                # code...
                $pedido->setCompSaldo(floatval($ctotali));
            }else{
                $pedido->setCompSaldo(floatval(0));
            }

            $em->persist($pedido);
            $em->flush();
        }
        $last_Id = $pedido->getId();
        $pediddd = $em->getRepository('App:Comprobante')->find($last_Id);
        if ($last_Id != '') {
           

            $last_Id2 =0;
        
               
            $caja = new Cajadiaria();
           // $caja->setPersona($idemclie);
            $caja->setComprobante($last_Id);
            $caja->setDescripcion('Ingreso - Comprobante Nº:'.$numero);
            $caja->setFecha($compfech);
            $caja->setDebe(floatval($ctotali));
            $caja->setHaber(floatval(0));
            $tipo = $em->getRepository('App:Tipocomp')->find($cventa);
            $caja->setTipoComprobante($tipo);
            $caja->setObservaciones($concept);
            $caja->setCompTimestamp($now);
            $em->persist($caja);
            $em->flush();
            $last_Id2 = $caja->getId();


            $status = array('id' => $last_Id,'caja'=>$last_Id2);


            //$status = array('id' => $last_Id);
        }
        return new JsonResponse($status);
    }

    

    /**
     * Recibo 
     *
     *
     * @Route("/pout", name="p_egreso")
     * @Method({"GET", "POST"})
     */
    public function nuevoegresoAction(Request $request)
    {
         $em = $this->getDoctrine()->getManager();
        $idemclie = $request->request->get("idclie");
        // $idemclie =1380;
        //        $idEmpl = 2;
        // $empl_id = $this->getUser()->getId();
        $ctotali = $request->request->get("ctotal");
        $compfech = $request->request->get("cfecha");
        $observ = $request->request->get("observ");
        //$largo = $request->request->get("largo");
        $cventa = $request->request->get("cventa");
        $concept = $request->request->get("concept");
        // $cenvio = $request->request->get("cenvio");
       // $datos = json_decode($_POST['array'], true);
        // $empleado = $em->getRepository('AppBundle:Empleado')->find( $empl_id);
        $persona = $em->getRepository('App:Persona')->find($idemclie);
        //$tipocomp = $em->getRepository('App:Tipocomp')->find(31);//compr nota de pedido
        $pedido = new Comprobante();

        $status = array('id' => '','caja'=>0);
        //        ($idemclie != '') and
        date_default_timezone_set('America/Argentina/Cordoba');
        $now = date("Y-m-d H:i:s");
        // SELECT MAX(`comp_numero`)+1 As numero FROM `comprobante` WHERE `tcmp_id`= 31

        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = "SELECT MAX(comp_numero)+1 As numero FROM comprobante WHERE tcmp_id= :tdoc "; //31 np edido 32 recibo 33 nota de credito de pedidos
        $stmt1 = $conn->prepare($sql);
        $params2 = array('tdoc' => $cventa);
        $stmt1->execute($params2);
        $stmt1->execute();

        $ultimo = $stmt1->fetchAll();

        $numero = strval($ultimo[0]['numero']);
        if ($numero == '' ) {
           $numero = '1';
        }
        if (($ctotali != '')) {
            $pedido->setLogiId(1);
            $pedido->setCliente($persona);
            $pedido->setCompObservaciones($concept);
            $pedido->setCompTotal(floatval($ctotali)); //pasar de string a float valor
            $pedido->setTcmpId($cventa);
            $pedido->setCompFecha($compfech);
            $pedido->setCompCtacte(0);
            $pedido->setCompFechaVenc($compfech);
            $pedido->setCompTimestamp($now);
            $pedido->setCompNumero($numero);
            // $pedido->setReparto($cenvio);
            if ($cventa == 1) {
                # code...
                $pedido->setCompSaldo(floatval($ctotali));
            }else{
                $pedido->setCompSaldo(floatval(0));
            }

            $em->persist($pedido);
            $em->flush();
        }
        $last_Id = $pedido->getId();
        $pediddd = $em->getRepository('App:Comprobante')->find($last_Id);
        if ($last_Id != '') {
           

            $last_Id2 =0;
        
               
            $caja = new Cajadiaria();
           // $caja->setPersona($idemclie);
            $caja->setComprobante($last_Id);
            $caja->setDescripcion('Egreso - Comprobante Nº:'.$numero);
            $caja->setFecha($compfech);
            $caja->setDebe(floatval(0));
            $caja->setHaber(floatval($ctotali));
            $tipo = $em->getRepository('App:Tipocomp')->find($cventa);
            $caja->setTipoComprobante($tipo);
            $caja->setObservaciones($concept);
            $caja->setCompTimestamp($now);
            $em->persist($caja);
            $em->flush();
            $last_Id2 = $caja->getId();


            $status = array('id' => $last_Id,'caja'=>$last_Id2);


            //$status = array('id' => $last_Id);
        }
        return new JsonResponse($status);
        
    }


}