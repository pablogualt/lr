<?php

namespace App\Controller;

use App\Entity\Comprobante;
use App\Form\ComprobanteType;
use App\Entity\DetacompFac;
use App\Form\DetacompFacType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/comprobante")
 */
class ComprobanteController extends AbstractController
{
    /**
     * @Route("/", name="comprobante_index", methods={"GET"})
     */
    public function index(): Response
    {
        $comprobantes = $this->getDoctrine()
            ->getRepository(Comprobante::class)
            ->findAll();


        return $this->render('comprobante/index.html.twig', [
            'comprobantes' => $comprobantes,
        ]);
    }

    /**
     * @Route("/new", name="comprobante_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        // $comprobante = new Comprobante();
        // $form = $this->createForm(ComprobanteType::class, $comprobante);
        // $form->handleRequest($request);

        // if ($form->isSubmitted() && $form->isValid()) {
        //     $entityManager = $this->getDoctrine()->getManager();
        //     $entityManager->persist($comprobante);
        //     $entityManager->flush();

        //     return $this->redirectToRoute('comprobante_index');
        // }
        $em = $this->getDoctrine()->getManager();
        $articulos = $em->getRepository('App:Articulo')->findAll();
        $personas = $em->getRepository('App:Persona')->findAll();
        // $zona = $em->getRepository('AppBundle:Zona')->findAll();
        return $this->render('comprobante/new.html.twig', [
            'personas' => $personas,
            'articulos' => $articulos
        ]);
    }

    /**
     * Creates a new pedidosweb entity.
     *
     * @Route("/new2", name="notacredito_new")
     * @Method({"GET", "POST"})
     */
    public function newcreditoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $articulos = $em->getRepository('App:Articulo')->findAll();
        $personas = $em->getRepository('App:Persona')->findAll();

        return $this->render('comprobante/new2.html.twig', array(
            //            'pedidosweb' => $pedidosweb,
            //            'form' => $form->createView(),
            'personas' => $personas,
            'articulos' => $articulos,

        ));
    }

    /**
     * Creates a new pedidosweb entity.
     *
     * @Route("/new2/{id}", name="notacredito_02")
     * @Method({"GET", "POST"})
     */
    public function newcreditoAction02(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $articulos = $em->getRepository('App:Articulo')->findAll();
        // $personas = $em->getRepository('App:Persona')->find($id);
        $persona = $em->getRepository('App:Persona')->find($id);
        return $this->render('comprobante/nc02.html.twig', array(
            //            'pedidosweb' => $pedidosweb,
            //            'form' => $form->createView(),
            'persona' => $persona,
            'articulos' => $articulos,

        ));
    }


    /**
     * Creates a new pedidosweb entity.
     *
     * @Route("/new3", name="recibo_new")
     * @Method({"GET", "POST"})
     */
    public function reciboAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $articulos = $em->getRepository('App:Articulo')->findAll();
        $personas = $em->getRepository('App:Persona')->findAll();

        return $this->render('comprobante/new3.html.twig', array(
            //            'pedidosweb' => $pedidosweb,
            //            'form' => $form->createView(),
            'personas' => $personas,
            'articulos' => $articulos,

        ));
    }
    // /**
    //  * @Route("/{id}", name="comprobante_show", methods={"GET"})
    //  */
    // public function show($id): Response
    // {
    //     $em = $this->getDoctrine()->getManager();
    //     //$deleteForm = $this->createDeleteForm($pedidosweb);
    //     $comprobante = $em->getRepository('App:Comprobante')->find($id);
    //     // $comprobantes = $this->getDoctrine()
    //     //     ->getRepository(Comprobante::class)
    //     //     ->find($id)();
    //     return $this->render('comprobante/show.html.twig', [
    //         'comprobante' => $comprobante,
    //     ]);
    // }

    /**
     * Finds and displays a pedidosweb entity.
     *
     * @Route("/{id}", name="comprobante_show")
     * @Method("GET")
     */
    public function showAction($id)
    {

        //$em = $this->getDoctrine()->getManager();
        //$deleteForm = $this->createDeleteForm($pedidosweb);
        $comprobantes = $this->getDoctrine()
            ->getRepository(Comprobante::class)
            ->find($id);
       // $pedidoswebs = $em->getRepository('AppBundle:Pedidosweb')->find($id);
        //$detapedidoswebs = $em->getRepository('App:DetacompFac')->findBy(array('comprobante' => $comprobantes));
        $detacompFacs = $this->getDoctrine()
            ->getRepository(DetacompFac::class)
            ->findBy(array('comprobante' => $comprobantes));


        return $this->render('comprobante/show.html.twig', array(
            'comprobante' => $comprobantes,

            'detacomp_facs' => $detacompFacs,
            //'delete_form' => $deleteForm->createView(),
        ));
    }




    // /**
    //  * @Route("/{id}", name="comprobante_delete", methods={"DELETE"})
    //  */
    // public function delete(Request $request, Comprobante $comprobante): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$comprobante->getId(), $request->request->get('_token'))) {
    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->remove($comprobante);
    //         $entityManager->flush();
    //     }

    //     return $this->redirectToRoute('comprobante_index');
    // }
}
