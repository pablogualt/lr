<?php

namespace App\Controller;

use App\Entity\Cheques3ros;
use App\Entity\Chqestado;
use App\Entity\Chq3destino;
use App\Form\Cheques3rosType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tercero")
 */
class Cheques3rosController extends AbstractController
{
    /**
     * @Route("/", name="cheques3ros_index", methods={"GET"})
     */
    public function index(): Response
    {
        $cheques3ros = $this->getDoctrine()
            ->getRepository(Cheques3ros::class)
            ->findAll();

        return $this->render('cheques3ros/index.html.twig', [
            'cheques3ros' => $cheques3ros,
        ]);
    }

    /**
     * @Route("/new", name="cheques3ros_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $cheques3ro = new Cheques3ros();
        $form = $this->createForm(Cheques3rosType::class, $cheques3ro);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cheques3ro);
            $entityManager->flush();

            return $this->redirectToRoute('cheques3ros_index');
        }

        return $this->render('cheques3ros/new.html.twig', [
            'cheques3ro' => $cheques3ro,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cheques3ros_show", methods={"GET"})
     */
    public function show(Cheques3ros $cheques3ro): Response
    {
        return $this->render('cheques3ros/show.html.twig', [
            'cheques3ro' => $cheques3ro,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="cheques3ros_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Cheques3ros $cheques3ro): Response
    {

        $chqestados = $this->getDoctrine()
            ->getRepository(Chqestado::class)
            ->findAll();
      $chqdestinos = $this->getDoctrine()
            ->getRepository(Chq3destino::class)
            ->findAll();
       

             if ($request->isMethod('POST')) {

                 $em = $this->getDoctrine()->getManager();
        $idchq = $request->request->get("id");
   
        $fsalida = $request->request->get("salida");
        $cestado = $request->request->get("estado");
        $cobserv = $request->request->get("observ");
        $cdestino = $request->request->get("destino");
       
        // $DestinoId = $em->getRepository('App:Chq3destino')->find($cdestino);
        // $EstadoId = $em->getRepository('App:Chqestado')->find($cestado);
        // $tipocomp = $em->getRepository('App:Tipocomp')->find(31);//compr nota de pedido
      
        date_default_timezone_set('America/Argentina/Cordoba');

        // $status = array('id' => '','caja'=>0);
        //        ($idemclie != '') and
        $now = date("Y-m-d H:i:s");

        $qb = $em->createQueryBuilder();
                    $q = $qb->update('App:Cheques3ros', 'c')
                            ->set('c.fechaDeSalida', '?1')
                             ->set('c.estado', '?3')
                             ->set('c.destino', '?4')
                             ->set('c.observaciones', '?5')
                            ->where('c.id = ?2')
                            ->setParameter(1, $fsalida)
                            ->setParameter(2, $idchq)
                            ->setParameter(3, $cestado)
                            ->setParameter(4, $cdestino)
                            ->setParameter(5, $cobserv.' - '.'Modificacion :'.$now)
                            ->getQuery();
                    $p = $q->execute();
      

         return $this->redirectToRoute('cheques3ros_show', array('id' => $idchq));
        // }
         }


        return $this->render('cheques3ros/edit.html.twig', [
            'cheques3ro' => $cheques3ro,
            'chqestados' => $chqestados,
            'chqdestinos' => $chqdestinos,
            // 'form' => $form->createView(),
        ]);
    }

    // /**
    //  * @Route("/{id}", name="cheques3ros_delete", methods={"DELETE"})
    //  */
    // public function delete(Request $request, Cheques3ros $cheques3ro): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$cheques3ro->getId(), $request->request->get('_token'))) {
    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->remove($cheques3ro);
    //         $entityManager->flush();
    //     }

    //     return $this->redirectToRoute('cheques3ros_index');
    // }
}
