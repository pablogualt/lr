<?php

namespace App\Controller;

use App\Entity\Chqestado;
use App\Form\ChqestadoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/chqestado")
 */
class ChqestadoController extends AbstractController
{
    /**
     * @Route("/", name="chqestado_index", methods={"GET"})
     */
    public function index(): Response
    {
        $chqestados = $this->getDoctrine()
            ->getRepository(Chqestado::class)
            ->findAll();

        return $this->render('chqestado/index.html.twig', [
            'chqestados' => $chqestados,
        ]);
    }

    /**
     * @Route("/new", name="chqestado_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $chqestado = new Chqestado();
        $form = $this->createForm(ChqestadoType::class, $chqestado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($chqestado);
            $entityManager->flush();

            return $this->redirectToRoute('chqestado_index');
        }

        return $this->render('chqestado/new.html.twig', [
            'chqestado' => $chqestado,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="chqestado_show", methods={"GET"})
     */
    public function show(Chqestado $chqestado): Response
    {
        return $this->render('chqestado/show.html.twig', [
            'chqestado' => $chqestado,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="chqestado_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Chqestado $chqestado): Response
    {
        $form = $this->createForm(ChqestadoType::class, $chqestado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('chqestado_index');
        }

        return $this->render('chqestado/edit.html.twig', [
            'chqestado' => $chqestado,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="chqestado_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Chqestado $chqestado): Response
    {
        if ($this->isCsrfTokenValid('delete'.$chqestado->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($chqestado);
            $entityManager->flush();
        }

        return $this->redirectToRoute('chqestado_index');
    }
}
