<?php

namespace App\Controller;
use App\Entity\Persona;
use App\Form\PersonaType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ResumenController extends AbstractController
{
        
    /**
     * @Route("/resumen", name="resumen")
     */
    public function index()
    {
         $conn = $this->getDoctrine()->getManager()->getConnection();
         date_default_timezone_set('America/Argentina/Cordoba');
         $now2 = date('Y-m-d', strtotime("+1 day"));
         $now1 = date("Y-m-d");//date_format($date,"Y/m/d H:i:s");

     


            $sql = " SELECT
                    articulo.arti_codigo_original As Cod_O,articulo.arti_codigo_alternativo As Cod_A,articulo.arti_codigo_propio As Cod_P,
                    detacomp_fac.prod_descri As Producto,
                    IF
                    ( comprobante.tcmp_id = 31 ,
                    detacomp_fac.prod_cant,0) As CantNP,
                    IF
                    ( comprobante.tcmp_id = 33 ,
                    detacomp_fac.prod_cant,0) As CantNC,
                    detacomp_fac.prod_pvp As Pvp, 
                    IF
                    ( comprobante.tcmp_id = 33 ,
                    (Round(detacomp_fac.prod_cant * (detacomp_fac.prod_pvp * - (1) ),2)),
                    (Round(detacomp_fac.prod_cant * detacomp_fac.prod_pvp ,2))) AS TOTAL
                    FROM
                    detacomp_fac
                    INNER JOIN comprobante ON comprobante.id = detacomp_fac.comp_id
                    LEFT OUTER JOIN articulo ON articulo.id = detacomp_fac.prod_id
                    WHERE
                    comprobante.tcmp_id IN (31, 33) AND
                    comprobante.comp_fecha BETWEEN :desde AND :hasta AND
                    comprobante.comp_ctacte = 0
                     ORDER BY
                   detacomp_fac.prod_descri ";
            $stmt1 = $conn->prepare($sql);
            $params2 = array('desde' => $now1, 'hasta' => $now2);
            $stmt1->execute($params2);
            // $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

            $query = " SELECT
                    articulo.arti_codigo_original As Cod_O,articulo.arti_codigo_alternativo As Cod_A,articulo.arti_codigo_propio As Cod_P,
                    detacomp_fac.prod_descri As Producto,
                    IF
                    ( comprobante.tcmp_id = 31 ,
                    detacomp_fac.prod_cant,0) As CantNP,
                    IF
                    ( comprobante.tcmp_id = 33 ,
                    detacomp_fac.prod_cant,0) As CantNC,
                    detacomp_fac.prod_pvp As Pvp, 
                    IF
                    ( comprobante.tcmp_id = 33 ,
                    (Round(detacomp_fac.prod_cant * (detacomp_fac.prod_pvp * - (1) ),2)),
                    (Round(detacomp_fac.prod_cant * detacomp_fac.prod_pvp ,2))) AS TOTAL
                    FROM
                    detacomp_fac
                    INNER JOIN comprobante ON comprobante.id = detacomp_fac.comp_id
                    LEFT OUTER JOIN articulo ON articulo.id = detacomp_fac.prod_id
                    WHERE
                    comprobante.tcmp_id IN (31, 33) AND
                    comprobante.comp_fecha BETWEEN :desde AND :hasta AND
                    comprobante.comp_ctacte = 1
                    ORDER BY
                   detacomp_fac.prod_descri ";
            $stmt2 = $conn->prepare($query);
              $params3 = array('desde' => $now1, 'hasta' => $now2);
            $stmt2->execute($params3);
        
            // $stmt2->execute();
            // returns an array of arrays (i.e. a raw data set)
            $dos = $stmt2->fetchAll();



        return $this->render('resumen/diario.html.twig', [
            'unos' => $uno,
            'doss' => $dos,
        ]);
    }


     /**
     *
     * @Route("/resumen5", name="resumen_efechas")
     *
     * @Method({"GET", "POST"})
     */
    public function index3(Request $request)
    {

         $now1 = $request->request->get("desde");
         $now2 = $request->request->get("hasta");
         $conn = $this->getDoctrine()->getManager()->getConnection();
         date_default_timezone_set('America/Argentina/Cordoba');
         // $now2 = date('Y-m-d', strtotime("+1 day"));
         // $now1 = date("Y-m-d");//date_format($date,"Y/m/d H:i:s");

     


            $sql = " SELECT
                    articulo.arti_codigo_original As Cod_O,articulo.arti_codigo_alternativo As Cod_A,articulo.arti_codigo_propio As Cod_P,
                    detacomp_fac.prod_descri As Producto,
                    IF
                    ( comprobante.tcmp_id = 31 ,
                    detacomp_fac.prod_cant,0) As CantNP,
                    IF
                    ( comprobante.tcmp_id = 33 ,
                    detacomp_fac.prod_cant,0) As CantNC,
                    detacomp_fac.prod_pvp As Pvp, 
                    IF
                    ( comprobante.tcmp_id = 33 ,
                    (Round(detacomp_fac.prod_cant * (detacomp_fac.prod_pvp * - (1) ),2)),
                    (Round(detacomp_fac.prod_cant * detacomp_fac.prod_pvp ,2))) AS TOTAL
                    FROM
                    detacomp_fac
                    INNER JOIN comprobante ON comprobante.id = detacomp_fac.comp_id
                    LEFT OUTER JOIN articulo ON articulo.id = detacomp_fac.prod_id
                    WHERE
                    comprobante.tcmp_id IN (31, 33) AND
                    comprobante.comp_fecha BETWEEN :desde AND :hasta AND
                    comprobante.comp_ctacte = 0
                     ORDER BY
                   detacomp_fac.prod_descri ";
            $stmt1 = $conn->prepare($sql);
            $params2 = array('desde' => $now1, 'hasta' => $now2);
            $stmt1->execute($params2);
            // $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

            $query = " SELECT
                    articulo.arti_codigo_original As Cod_O,articulo.arti_codigo_alternativo As Cod_A,articulo.arti_codigo_propio As Cod_P,
                    detacomp_fac.prod_descri As Producto,
                    IF
                    ( comprobante.tcmp_id = 31 ,
                    detacomp_fac.prod_cant,0) As CantNP,
                    IF
                    ( comprobante.tcmp_id = 33 ,
                    detacomp_fac.prod_cant,0) As CantNC,
                    detacomp_fac.prod_pvp As Pvp, 
                    IF
                    ( comprobante.tcmp_id = 33 ,
                    (Round(detacomp_fac.prod_cant * (detacomp_fac.prod_pvp * - (1) ),2)),
                    (Round(detacomp_fac.prod_cant * detacomp_fac.prod_pvp ,2))) AS TOTAL
                    FROM
                    detacomp_fac
                    INNER JOIN comprobante ON comprobante.id = detacomp_fac.comp_id
                    LEFT OUTER JOIN articulo ON articulo.id = detacomp_fac.prod_id
                    WHERE
                    comprobante.tcmp_id IN (31, 33) AND
                    comprobante.comp_fecha BETWEEN :desde AND :hasta AND
                    comprobante.comp_ctacte = 1
                    ORDER BY
                   detacomp_fac.prod_descri ";
            $stmt2 = $conn->prepare($query);
              $params3 = array('desde' => $now1, 'hasta' => $now2);
            $stmt2->execute($params3);
        
            // $stmt2->execute();
            // returns an array of arrays (i.e. a raw data set)
            $dos = $stmt2->fetchAll();



        return $this->render('resumen/diario.html.twig', [
            'unos' => $uno,
            'doss' => $dos,
        ]);
    }

   

    /**
     * @Route("/resumen3", name="resumen_general")
     */
    public function indexgeneral()
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();

            $sql = " SELECT
                    persona.id AS ID,
                    persona.pers_nombre AS CLIENTE,
                    Sum(IF (
                        comprobante.tcmp_id in( 31),
                        (ROUND(comprobante.comp_saldo, 2)),
                        (ROUND(0,2))
                    )) AS DEBE,
                    Sum(IF (
                        comprobante.tcmp_id in( 33,32),
                            (ROUND(comprobante.comp_saldo, 2)),
                        (ROUND(0,2))
                    )) AS HABER,
                    Sum(IF (
                        comprobante.tcmp_id in( 33,32),
                            (ROUND(comprobante.comp_saldo * (-1), 2)),
                        (ROUND(comprobante.comp_saldo, 2))
                    )) AS SALDO
                    ,

                        max( comprobante.comp_fecha)
                     AS MOVIMIENTO

                    FROM
                    comprobante 
                    INNER JOIN persona ON persona.id = comprobante.pers_id
                    INNER JOIN tipocomp ON tipocomp.id = comprobante.tcmp_id
                    WHERE

                    comprobante.comp_ctacte = 1 AND
                    comprobante.comp_apro = 0
                    GROUP BY
                    persona.pers_nombre
            ";
            $stmt1 = $conn->prepare($sql);
            //$params2 = array('nom' => $result, 'yeari' => intval($cinco), 'hoy' => intval($now2));
            //$stmt1->execute($params2);
            $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

            

        return $this->render('resumen/general.html.twig', [
            'unos' => $uno
            // 'controller_name' => 'ResumenController',
        ]);
    }

    /**
     * @Route("/resumen4", name="resumen_seleccion")
     */
    public function indexseleccion()
    {
        return $this->render('resumen/seleccion.html.twig', [
            // 'controller_name' => 'ResumenController',
        ]);
    }
    /**
     * @Route("/resumen/{id}", name="resumen_individual")
     */
    public function indexseleccionindi($id)
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $sql = " SELECT
            comprobante.id ,
            tipocomp.tcmp_nshort AS TIPO,
            comprobante.comp_numero AS NUMERO,
            comprobante.comp_fecha AS FECHA,
            persona.pers_nombre AS CLIENTE,
    
            IF (
                comprobante.tcmp_id in( 31),
                (ROUND(comprobante.comp_saldo, 2)),
                (ROUND(0,2))
            ) AS DEBE
            ,

            IF (
                comprobante.tcmp_id in( 33,32),
                    (ROUND(comprobante.comp_saldo, 2)),
                (ROUND(0,2))
            ) AS HABER
            ,
            IF(comprobante.comp_apro=0, 'NO', 'SI') AS APROPIADO
            ,comprobante.comp_total AS TOTAL
            FROM
            comprobante 
            INNER JOIN persona ON persona.id = comprobante.pers_id
            INNER JOIN tipocomp ON tipocomp.id = comprobante.tcmp_id
            WHERE
            
            comprobante.pers_id = :idem
            AND
            comprobante.comp_ctacte = 1
            AND
            comprobante.comp_apro = 0
            ORDER BY comprobante.comp_fecha
            ";
// comprobante.comp_fecha BETWEEN "2019-08-15" AND "2019-08-30" AND
            $stmt1 = $conn->prepare($sql);
            $params2 = array('idem' => $id );
            $stmt1->execute($params2);
            //$stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

            $persona = $this->getDoctrine()
            ->getRepository(Persona::class)
             ->find($id);
        return $this->render('resumen/individual.html.twig', [
            'unos' => $uno,
            'persona' => $persona,
        ]);
    }

    /**
     * @Route("/resumen/{id}/all", name="resumen_ind_all")
     */
    public function indexseleccionindiall($id)
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $sql = " SELECT
            comprobante.id ,
            tipocomp.tcmp_nshort AS TIPO,
            comprobante.comp_numero AS NUMERO,
            comprobante.comp_fecha AS FECHA,
            persona.pers_nombre AS CLIENTE,
    
            IF (
                comprobante.tcmp_id in( 31),
                (ROUND(comprobante.comp_saldo, 2)),
                (ROUND(0,2))
            ) AS DEBE
            ,

            IF (
                comprobante.tcmp_id in( 33,32),
                    (ROUND(comprobante.comp_saldo, 2)),
                (ROUND(0,2))
            ) AS HABER
            ,
            IF(comprobante.comp_apro=0, 'NO', 'SI') AS APROPIADO
            ,comprobante.comp_total AS TOTAL
            FROM
            comprobante 
            INNER JOIN persona ON persona.id = comprobante.pers_id
            INNER JOIN tipocomp ON tipocomp.id = comprobante.tcmp_id
            WHERE
            
            comprobante.pers_id = :idem
            AND
            comprobante.comp_ctacte = 1
            
            ORDER BY comprobante.comp_fecha
            ";
// comprobante.comp_fecha BETWEEN "2019-08-15" AND "2019-08-30" AND
            $stmt1 = $conn->prepare($sql);
            $params2 = array('idem' => $id );
            $stmt1->execute($params2);
            //$stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

            $persona = $this->getDoctrine()
            ->getRepository(Persona::class)
             ->find($id);
        return $this->render('resumen/individual.html.twig', [
            'unos' => $uno,
            'persona' => $persona,
        ]);
    }

    /**
     * @Route("/resumen/{id}/historial", name="resumen_historial")
     */
    public function indexgeneralreclamo($id)
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();

            $sql = " SELECT
                    comp_fecha AS Fecha,
                        detacomp_fac.prod_descri AS Producto

                    FROM
                        detacomp_fac
                    INNER JOIN comprobante ON comprobante.id = detacomp_fac.comp_id
                    Left outer JOIN articulo ON articulo.id = detacomp_fac.prod_id
                    WHERE
                     comprobante.comp_ctacte = 1 and pers_id =:idem
                    and comprobante.tcmp_id = 31
                    ORDER BY
                    comp_fecha DESC
            ";
            $stmt1 = $conn->prepare($sql);
             $params2 = array('idem' => $id );
              $stmt1->execute($params2);
            //$params2 = array('nom' => $result, 'yeari' => intval($cinco), 'hoy' => intval($now2));
            //$stmt1->execute($params2);
         
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

            

        return $this->render('resumen/historial.html.twig', [
            'unos' => $uno
            // 'controller_name' => 'ResumenController',
        ]);
    }
}
