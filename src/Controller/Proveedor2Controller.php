<?php

namespace App\Controller;

use App\Entity\Proveedor2;
use App\Form\Proveedor2Type;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/proveedor")
 */
class Proveedor2Controller extends AbstractController
{
    /**
     * @Route("/", name="proveedor2_index", methods={"GET"})
     */
    public function index(): Response
    {
        $proveedor2s = $this->getDoctrine()
            ->getRepository(Proveedor2::class)
            ->findAll();

        return $this->render('proveedor2/index.html.twig', [
            'proveedor2s' => $proveedor2s,
        ]);
    }

    /**
     * @Route("/new", name="proveedor2_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $proveedor2 = new Proveedor2();
        $form = $this->createForm(Proveedor2Type::class, $proveedor2);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($proveedor2);
            $entityManager->flush();

            return $this->redirectToRoute('proveedor2_index');
        }

        return $this->render('proveedor2/new.html.twig', [
            'proveedor2' => $proveedor2,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="proveedor2_show", methods={"GET"})
     */
    public function show(Proveedor2 $proveedor2): Response
    {
        return $this->render('proveedor2/show.html.twig', [
            'proveedor2' => $proveedor2,
        ]);
    }

    // /**
    //  * @Route("/{id}/edit", name="proveedor2_edit", methods={"GET","POST"})
    //  */
    // public function edit(Request $request, Proveedor2 $proveedor2): Response
    // {
    //     $form = $this->createForm(Proveedor2Type::class, $proveedor2);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $this->getDoctrine()->getManager()->flush();

    //         return $this->redirectToRoute('proveedor2_index');
    //     }

    //     return $this->render('proveedor2/edit.html.twig', [
    //         'proveedor2' => $proveedor2,
    //         'form' => $form->createView(),
    //     ]);
    // }

    // /**
    //  * @Route("/{id}", name="proveedor2_delete", methods={"DELETE"})
    //  */
    // public function delete(Request $request, Proveedor2 $proveedor2): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$proveedor2->getId(), $request->request->get('_token'))) {
    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->remove($proveedor2);
    //         $entityManager->flush();
    //     }

    //     return $this->redirectToRoute('proveedor2_index');
    // }
}
