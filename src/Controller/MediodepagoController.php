<?php

namespace App\Controller;

use App\Entity\Mediodepago;
use App\Form\MediodepagoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mediodepago")
 */
class MediodepagoController extends AbstractController
{
    /**
     * @Route("/", name="mediodepago_index", methods={"GET"})
     */
    public function index(): Response
    {
        $mediodepagos = $this->getDoctrine()
            ->getRepository(Mediodepago::class)
            ->findAll();

        return $this->render('mediodepago/index.html.twig', [
            'mediodepagos' => $mediodepagos,
        ]);
    }

    /**
     * @Route("/new", name="mediodepago_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $mediodepago = new Mediodepago();
        $form = $this->createForm(MediodepagoType::class, $mediodepago);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mediodepago);
            $entityManager->flush();

            return $this->redirectToRoute('mediodepago_index');
        }

        return $this->render('mediodepago/new.html.twig', [
            'mediodepago' => $mediodepago,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mediodepago_show", methods={"GET"})
     */
    public function show(Mediodepago $mediodepago): Response
    {
        return $this->render('mediodepago/show.html.twig', [
            'mediodepago' => $mediodepago,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="mediodepago_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Mediodepago $mediodepago): Response
    {
        $form = $this->createForm(MediodepagoType::class, $mediodepago);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mediodepago_index');
        }

        return $this->render('mediodepago/edit.html.twig', [
            'mediodepago' => $mediodepago,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mediodepago_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Mediodepago $mediodepago): Response
    {
        if ($this->isCsrfTokenValid('delete'.$mediodepago->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($mediodepago);
            $entityManager->flush();
        }

        return $this->redirectToRoute('mediodepago_index');
    }
}
