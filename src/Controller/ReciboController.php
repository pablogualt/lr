<?php

namespace App\Controller;


use App\Entity\Comprobante;
use App\Form\ComprobanteType;
use App\Entity\DetacompFac;
use App\Entity\DetacompRec;
use App\Entity\Persona;
use App\Form\PersonaType;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ReciboController extends AbstractController
{
    /**
     * @Route("/recibo", name="recibo")
     */
    public function index()
    {
        return $this->render('recibo/index.html.twig', [
            'controller_name' => 'ReciboController',
        ]);
    }
    /**
     * Creates a new pedidosweb entity.
     *
     * @Route("/recibo/in", name="recibo_ingreso")
     * @Method({"GET", "POST"})
     */
    public function reciboAction3(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        // $articulos = $em->getRepository('App:Articulo')->findAll();
        // $personas = $em->getRepository('App:Persona')->findAll();

        return $this->render('recibo/ingresos.html.twig', array(
            //            'pedidosweb' => $pedidosweb,
            //            'form' => $form->createView(),
            // 'personas' => $personas,
            // 'articulos' => $articulos,

        ));
    }

    /**
     * Creates a new pedidosweb entity.
     *
     * @Route("/recibo/out", name="recibo_egreso")
     * @Method({"GET", "POST"})
     */
    public function reciboAction2(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        // $articulos = $em->getRepository('App:Articulo')->findAll();
        $personas = $em->getRepository('App:Persona')->findAll();

        return $this->render('recibo/egresos.html.twig', array(
            //            'pedidosweb' => $pedidosweb,
            //            'form' => $form->createView(),
            'personas' => $personas,
            // 'articulos' => $articulos,

        ));
    }



    /**
     * Finds and displays a pedidosweb entity.
     *
     * @Route("/recibo/{id}", name="recibo_show2")
     * @Method("GET")
     */
    public function showAction2($id)
    {

        //$em = $this->getDoctrine()->getManager();
        //$deleteForm = $this->createDeleteForm($pedidosweb);
        $comprobantes = $this->getDoctrine()
            ->getRepository(Comprobante::class)
            ->find($id);
       // $pedidoswebs = $em->getRepository('AppBundle:Pedidosweb')->find($id);
        //$detapedidoswebs = $em->getRepository('App:DetacompFac')->findBy(array('comprobante' => $comprobantes));
        $DetacompRecs = $this->getDoctrine()
            ->getRepository(DetacompRec::class)
            ->findBy(array('comprobante' => $comprobantes));


        return $this->render('recibo/show.html.twig', array(
            'comprobante' => $comprobantes,

            'detacomp_recs' => $DetacompRecs,
            //'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/recibo/{id}/impresion", name="pdf_recibo", methods={"GET"})
     * 
     */
    public function indexAction2(Request $request,$id)
    {
// @ParamDecryptor(params={"id"})
        // $idem = $this->get('nzo_url_encryptor')->decrypt($id);
        // $em = $this->getDoctrine()->getManager('customer');
        // $comprobante = $em->getRepository('App:Comprobanteweb')->find($id);

        // $deta = $em->getRepository('App:Detacompweb')->findBy(array('comprobante'=>$id));
       $em = $this->getDoctrine()->getManager();
        //$deleteForm = $this->createDeleteForm($pedidosweb);
        $comprobantes = $this->getDoctrine()
            ->getRepository(Comprobante::class)
            ->find($id);
       // $pedidoswebs = $em->getRepository('AppBundle:Pedidosweb')->find($id);
        //$detapedidoswebs = $em->getRepository('App:DetacompFac')->findBy(array('comprobante' => $comprobantes));
        $DetacompRecs = $this->getDoctrine()
            ->getRepository(DetacompRec::class)
            ->findBy(array('comprobante' => $comprobantes));
           
// return $this->render('comprobante/show.html.twig', array(
//             'comprobante' => $comprobantes,

//             'detacomp_facs' => $detacompFacs,
//             //'delete_form' => $deleteForm->createView(),
//         ));

        $html = $this->renderView('Impresion/pdfrecibo.twig',array(
                'comprobante' => $comprobantes,

             'detacomp_recs' => $DetacompRecs,
            ));

        $this->returnPDFAutomotor($html  );
    }

    public function returnPDFAutomotor($html)
    {
        $pdf = new \jonasarts\Bundle\TCPDFBundle\TCPDF\TCPDF('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
       // create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetCreator('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
 $pdf->SetAuthor('Recibo');
        $pdf->SetTitle('Recibo');//'Orden de Servicio Tecnico'
        $pdf->SetSubject('');
        $pdf->setFontSubsetting(true);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
 $style = array(
            'position' => '',
            'align' => 'L',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => 'L',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        $style2 = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => 'C',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0);
        $pdf->SetDrawColor(1, 0, 0);
        $pdf->SetLineWidth(0.3);
//    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.'Prueba', PDF_HEADER_STRING);
        $pdf->SetFont('helvetica', '', 9, '', true);
		// Add a page
		// This method has several options, check the source code documentation for more information.
		// $pdf->AddPage();
		$pdf->AddPage('L', 'A5');
		// set text shadow effect
		// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


		 $pdf->Line(5, 140, 205, 140, $style);
		//        $pdf->Cell(0, 0, 'Orden de Servicio Tecnico', 0, 1);
		 $pdf->Cell(0, 0, 'Original', 1, 1, 'C');
		 $pdf->Ln(2);
		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');
		 $pdf->Ln(8);
		 $pdf->AddPage('L', 'A5');
		  // $pdf->Line(5,5);
		   // $pdf->Line(0, 5, 230, 5, $style);
		 $pdf->Cell(0, 0, 'Duplicado', 1, 1, 'C');
		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');
		// $pdf->write1DBarcode($codigointerno, 'C39', '', '', '', 18, 0.4, $style2, 'R');//genera codigo de barra
		$pdf->Line(5, 140, 205, 140, $style);
		//    $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

		// write some JavaScript code
		// $js = <<<EOD
		// app.alert('JavaScript Popup Example', 3, 0, 'Welcome');
		// var cResponse = app.response({
		//     cQuestion: 'How are you today?',
		//     cTitle: 'Your Health Status',
		//     cDefault: 'Fine',
		//     cLabel: 'Response:'
		// });
		// if (cResponse == null) {
		//     app.alert('Thanks for trying anyway.', 3, 0, 'Result');
		// } else {
		//     app.alert('You responded, "'+cResponse+'", to the health question.', 3, 0, 'Result');
		// }
		// EOD;

		// force print dialog
		// $js .= 'print(true);';

		// set javascript
		 $pdf->IncludeJS('print(true);');




		$hora = date('d-m-Y_H-i-s');
		$filename = 'Recibo_'.$hora;
		$pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
		}

     /**
     *
     * @Route("/recibo/{id}/cuenta", name="RecCuenta", methods={"GET"})
     * 
     */
    public function indexAction22(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        // $articulos = $em->getRepository('App:Articulo')->findAll();
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $persona = $em->getRepository('App:Persona')->find($id);

        $sql = " SELECT
                comprobante.id,
                    tipocomp.tcmp_nshort AS TIPO,
                    comprobante.comp_numero AS NUMERO,
                    comprobante.comp_fecha AS FECHA,
                    persona.pers_nombre AS CLIENTE,
                    
                IF (
                    comprobante.tcmp_id in( 31),
                    (ROUND(comprobante.comp_total, 2)),
                    (ROUND(comprobante.comp_total * (-1), 2))
                ) AS TOTAL
                ,


                IF (
                    comprobante.tcmp_id in( 31),
                    (ROUND(comprobante.comp_saldo, 2)),
                    (ROUND(comprobante.comp_saldo * (-1), 2))
                ) AS SALDO
                ,


                IF(comprobante.comp_apro=0, 'NO', 'SI') AS APROPIADO
                FROM
                comprobante 
                INNER JOIN persona ON persona.id = comprobante.pers_id
                INNER JOIN tipocomp ON tipocomp.id = comprobante.tcmp_id
                WHERE
                comprobante.pers_id = :idem 
                AND 
                comprobante.comp_ctacte = 1
                AND
                comprobante.comp_apro = 0
                ORDER BY comprobante.comp_fecha ";
            $stmt1 = $conn->prepare($sql);
             $params2 = array('idem' => $id);
            $stmt1->execute($params2);
            // $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

        return $this->render('recibo/new3.html.twig', array(
            //            'pedidosweb' => $pedidosweb,
            //            'form' => $form->createView(),
            'persona' => $persona,
            'unos' => $uno,
            // 'articulos' => $articulos,

        ));
    }



}
