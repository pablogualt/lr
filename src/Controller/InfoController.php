<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/info")
 */
class InfoController extends AbstractController
{
    /**
     * @Route("/", name="info")
     */
    public function index()
    {
        return $this->render('info/index.html.twig', [
            'controller_name' => 'InfoController',
        ]);
    }


     /**
     *
     * @Route("/ventas", name="info_ventas")
     *
     * @Method({"GET", "POST"})
     */
    public function index44(Request $request)
    {

         $now1 = $request->request->get("desde");
         $now2 = $request->request->get("hasta");
         $conn = $this->getDoctrine()->getManager()->getConnection();
         date_default_timezone_set('America/Argentina/Cordoba');
         // $now2 = date('Y-m-d', strtotime("+1 day"));
         // $now1 = date("Y-m-d");//date_format($date,"Y/m/d H:i:s");

     


            $sql = "SELECT
			comprobante.comp_fecha AS FECHA,
			tipocomp.tcmp_denomina AS TIPOCOMP,
			comprobante.comp_numero AS NUMERO,
			persona.pers_nombre AS CLIENTE,
			persona.pers_cuit AS CUIT,
			IF
			( comprobante.comp_ctacte = 1, 'CTACTE', 'CONTADO' ) AS TIPO_VENTA,
			IF
			(
				comprobante.comp_ctacte = 1,
			IF
				(
					comprobante.comp_total = comprobante.comp_saldo,
					'SALDADO',
				CONCAT( 'ADEUDA : $', comprobante.comp_total - comprobante.comp_saldo )),
				'CONTADO' 
			) AS ESTADO,
			comprobante.comp_total AS TOTAL 
			FROM
			comprobante
			INNER JOIN persona ON persona.id = comprobante.pers_id
			INNER JOIN tipocomp ON tipocomp.id = comprobante.tcmp_id 
			WHERE
			comprobante.tcmp_id = 31  AND
			-- comprobante.comp_fecha BETWEEN '2020-03-01' AND '2020-04-01'
			comprobante.comp_fecha BETWEEN :desde AND :hasta 
			ORDER BY
			comprobante.id DESC  "; 

            $stmt1 = $conn->prepare($sql);
            $params2 = array('desde' => $now1, 'hasta' => $now2);
            $stmt1->execute($params2);
            // $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $venta = $stmt1->fetchAll();

            


        return $this->render('info/ventas.html.twig', [
            'unos' => $venta,
         
        ]);
    }

     /**
     *
     * @Route("/compras", name="info_compras")
     *
     * @Method({"GET", "POST"})
     */
    public function incompras(Request $request)
    {

         $now1 = $request->request->get("desde");
         $now2 = $request->request->get("hasta");
         $conn = $this->getDoctrine()->getManager()->getConnection();
         date_default_timezone_set('America/Argentina/Cordoba');
         // $now2 = date('Y-m-d', strtotime("+1 day"));
         // $now1 = date("Y-m-d");//date_format($date,"Y/m/d H:i:s");

     


            $sql = "SELECT
			comproprovee.comp_fecha AS FECHA,
			tipocomp.tcmp_denomina AS TIPOCOMP,
			comproprovee.comp_pdvynumero AS NUMERO,
			proveedor_full.pers_nombre AS PROVEEDOR,
			proveedor_full.pers_cuit AS CUIT,
			IF(comproprovee.comp_ctacte = 1,'CTACTE','CONTADO') AS TIPO_VENTA,
			IF(comproprovee.comp_ctacte = 1,IF(comproprovee.comp_total =comproprovee.comp_saldo,'SALDADO',CONCAT('ADEUDA : $', comproprovee.comp_saldo)),'CONTADO') AS ESTADO,
			comproprovee.comp_total AS TOTAL
			FROM
			comproprovee
			INNER JOIN proveedor_full ON proveedor_full.id = comproprovee.prove_id
			INNER JOIN tipocomp ON tipocomp.id = comproprovee.tcmp_id
			WHERE
			comproprovee.tcmp_id = 31  AND
			-- comprobante.comp_fecha BETWEEN '2020-03-01' AND '2020-04-01'
			comproprovee.comp_fecha BETWEEN :desde AND :hasta 
		
			ORDER BY
			comproprovee.id DESC  "; 

            $stmt1 = $conn->prepare($sql);
            $params2 = array('desde' => $now1, 'hasta' => $now2);
            $stmt1->execute($params2);
            // $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $venta = $stmt1->fetchAll();

            


        return $this->render('info/compras.html.twig', [
            'unos' => $venta,
         
        ]);
    }

     /**
     *
     * @Route("/gastos", name="info_gastos")
     *
     * @Method({"GET", "POST"})
     */
    public function indGastos(Request $request)
    {

         $now1 = $request->request->get("desde");
         $now2 = $request->request->get("hasta");
         $conn = $this->getDoctrine()->getManager()->getConnection();
         date_default_timezone_set('America/Argentina/Cordoba');
         // $now2 = date('Y-m-d', strtotime("+1 day"));
         // $now1 = date("Y-m-d");//date_format($date,"Y/m/d H:i:s");

     


            $sql = "SELECT
			comprobante.comp_fecha AS FECHA,
			tipocomp.tcmp_denomina AS TIPOCOMP,
			comprobante.comp_numero AS NUMERO,
			persona.pers_nombre AS CLIENTE,
			persona.pers_cuit AS CUIT,
			IF(comprobante.comp_ctacte = 1,'CTACTE','CONTADO') AS TIPO_VENTA,
			IF(comprobante.comp_ctacte = 1,IF(comprobante.comp_total =comprobante.comp_saldo,'SALDADO',CONCAT('ADEUDA : $',comprobante.comp_total - comprobante.comp_saldo)),'CONTADO') AS ESTADO,
			comprobante.comp_total AS TOTAL



			FROM
			comprobante
			INNER JOIN persona ON persona.id = comprobante.pers_id
			INNER JOIN tipocomp ON tipocomp.id = comprobante.tcmp_id
			WHERE
			comprobante.tcmp_id IN (34,35,36,37) 
			 AND
			-- comprobante.comp_fecha BETWEEN '2020-03-01' AND '2020-04-01'
			comprobante.comp_fecha BETWEEN :desde AND :hasta 
			ORDER BY
			comprobante.id DESC  "; 

            $stmt1 = $conn->prepare($sql);
            $params2 = array('desde' => $now1, 'hasta' => $now2);
            $stmt1->execute($params2);
            // $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $venta = $stmt1->fetchAll();

            


        return $this->render('info/gastos.html.twig', [
            'unos' => $venta,
         
        ]);
    }

}
