<?php

namespace App\Controller;

use App\Entity\Comproprovee;
use App\Form\ComproproveeType;
use App\Entity\Cajadiaria;
use App\Entity\Proveedor2;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

// use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/comproprovee")
 */
class ComproproveeController extends AbstractController
{
    /**
     * @Route("/", name="comproprovee_index", methods={"GET"})
     */
    public function index(): Response
    {
        $comproprovees = $this->getDoctrine()
            ->getRepository(Comproprovee::class)
            ->findAll();

        return $this->render('comproprovee/index.html.twig', [
            'comproprovees' => $comproprovees,
        ]);
    }

    /**
     * @Route("/new", name="comproprovee_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        // $comproprovee = new Comproprovee();
        // $form = $this->createForm(ComproproveeType::class, $comproprovee);
        // $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        // $articulos = $em->getRepository('App:Articulo')->findAll();
        $personas = $em->getRepository('App:Proveedor2')->findAll();
        // $zona = $em->getRepository('AppBundle:Zona')->findAll();
        // return $this->render('comprobante/new.html.twig', [
        //     'personas' => $personas,
        //     'articulos' => $articulos

        // if ($form->isSubmitted() && $form->isValid()) {
        //     $entityManager = $this->getDoctrine()->getManager();
        //     $entityManager->persist($comproprovee);
        //     $entityManager->flush();

        //     return $this->redirectToRoute('comproprovee_index');
        // }

        return $this->render('comproprovee/new.html.twig', [
           'personas' => $personas,
           
        ]);
    }

    /**
     * @Route("/{id}", name="comproprovee_show", methods={"GET"})
     */
    public function show(Comproprovee $comproprovee): Response
    {
        return $this->render('comproprovee/show.html.twig', [
            'comproprovee' => $comproprovee,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="comproprovee_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Comproprovee $comproprovee): Response
    {
        $form = $this->createForm(ComproproveeType::class, $comproprovee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comproprovee_index');
        }

        return $this->render('comproprovee/edit.html.twig', [
            'comproprovee' => $comproprovee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a new itecnico entity.
     *
     *
     * @Route("/pnew", name="comprove_nuevo", methods={"GET","POST"})
     */
    public function nuevoCompobantection(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idemclie = $request->request->get("idclie");
       // $idemclie =1380;
//        $idEmpl = 2;
        // $empl_id = $this->getUser()->getId();
        $ctotali = $request->request->get("ctotal");
        $compfech = $request->request->get("cfecha");
        $compfechVencimiento = $request->request->get("cfecha2");
        $observ = $request->request->get("observ");
       
        $cventa = $request->request->get("cventa");
        $tcomp = $request->request->get("tcomp");
        $numero = $request->request->get("numero");
      
        // $empleado = $em->getRepository('AppBundle:Empleado')->find( $empl_id);
        $proveedor = $em->getRepository('App:Proveedor2')->find($idemclie);
        $tipocomp = $em->getRepository('App:Tipocomp')->find($tcomp);//compr nota de pedido
        $comp = new Comproprovee();
        date_default_timezone_set('America/Argentina/Cordoba');

        $status = array('id' => '','caja'=>0);
        //        ($idemclie != '') and
        $now = date("Y-m-d H:i:s");
        // SELECT MAX(`comp_numero`)+1 As numero FROM `comprobante` WHERE `tcmp_id`= 31

        // $conn = $this->getDoctrine()->getManager()->getConnection();

        // $sql = "SELECT MAX(comp_numero +1) As numero FROM comprobante WHERE tcmp_id= 31
        // ";
        // $stmt1 = $conn->prepare($sql);
        // // $params2 = array('nom' => $result, 'yeari' => intval($cinco), 'hoy' => intval($now2));
        // $stmt1->execute();
        // $ultimo = $stmt1->fetchAll();
        // $numero = strval($ultimo[0]['numero']);
        // if ($numero == '' ) {
        //    $numero = '1';
        // }
       
        
        if(($ctotali != '')) {
            $comp->setLogiId(1);
            $comp->setProveedor($proveedor);
            $comp->setObservaciones($observ);
            $comp->setTotal(floatval($ctotali)); //pasar de string a float valor
            $comp->setTipoComprobante($tipocomp);//
            $comp->setFecha($compfech);
            $comp->setCtacte($cventa);
            $comp->setFechaVenc($compfech);
            $comp->setCompFechacarga($now);
            $comp->setPdvynumero($numero);
            if ($cventa == 1) { //si es cuenta corriente 
                # code...
                $comp->setSaldo(floatval($ctotali));// $comp->setTotal(floatval($ctotali));
                $comp->setApropiado(0);
            }else{ //si es contado 
                $comp->setSaldo(floatval(0));
                $comp->setApropiado(1);
            }
            
           
                    // $pedido->setReparto($cenvio);


            $em->persist($comp);
            $em->flush();

        }
            $last_Id = $comp->getId();
            // $pediddd = $em->getRepository('App:Comproprovee')->find($last_Id);
        if ($last_Id != ''){
       
            //cajadiaria aca 
            $last_Id2 =0;

            // if ($tcomp == 25) {// 25 orden de pago 
            //    $caja = new Cajadiaria();
            //        // $caja->setPersona($idemclie);
            //         $caja->setComprobante($last_Id);
            //         $caja->setDescripcion('Egreso - ORDEN DE PAGO Nº:'.$last_Id);
            //         $caja->setFecha($compfech);
            //         $caja->setDebe(floatval(0));
            //         $caja->setHaber(floatval($ctotali));
            //         // $tipo = $em->getRepository('App:Tipocomp')->find(31);
            //         $caja->setTipoComprobante($tipocomp);
            //         $caja->setCompTimestamp($now);
            //         $caja->setObservaciones('Pago a Proveedor : '. $proveedor->getNombre().' - Descripcion :  '.$observ);
            //         $em->persist($caja);
            //         $em->flush();
            //         $last_Id2 = $caja->getId();
            // }
           // if ($cventa == 0) {
           //      if ($last_Id != ''){
           //          $caja = new Cajadiaria();
           //         // $caja->setPersona($idemclie);
           //          $caja->setComprobante($last_Id);
           //          $caja->setDescripcion('Ingreso - Nota de Pedido Nº:'.$numero);
           //          $caja->setFecha($compfech);
           //          $caja->setDebe(floatval($ctotali));
           //          $caja->setHaber(floatval(0));
           //          $tipo = $em->getRepository('App:Tipocomp')->find(31);
           //          $caja->setTipoComprobante($tipo);
           //          $caja->setCompTimestamp($now);
           //          $em->persist($caja);
           //          $em->flush();
           //          $last_Id2 = $caja->getId();


           //      }
                
           //  }



            $status = array('id' => $last_Id,'caja'=>$last_Id2);
       }
        return new JsonResponse($status);
    }


     /**
     *
     * @Route("/cuenta/general", name="comprove_general", methods={"GET"})
     */
    public function indexGeneral(): Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();//listo comprobado

            $sql = " SELECT
                    proveedor_full.id AS ID,
                    proveedor_full.pers_nombre AS PROVEEDOR,
                    Sum(IF (
                        

                        comproprovee.tcmp_id in( 33,25),
                            (ROUND(comproprovee.comp_total, 2)),
                        (ROUND(0,2))
                    )) AS DEBE,
                    Sum(IF (
                        comproprovee.tcmp_id in( 31),
                        (ROUND(comproprovee.comp_total, 2)),
                        (ROUND(0,2))
                    )) AS HABER,
                    Sum(IF (
                        comproprovee.tcmp_id in( 33,25),
                            
                        (ROUND(comproprovee.comp_total, 2)),
                        (ROUND(comproprovee.comp_total * (-1), 2))
                    )) AS SALDO 
                    ,

                        max( comproprovee.comp_fecha)
                     AS MOVIMIENTO

                    FROM
                    comproprovee 
                    INNER JOIN proveedor_full ON proveedor_full.id = comproprovee.prove_id
                    INNER JOIN tipocomp ON tipocomp.id = comproprovee.tcmp_id
                    WHERE

                    comproprovee.comp_ctacte = 1
                    GROUP BY
                    proveedor_full.pers_nombre
            ";
            $stmt1 = $conn->prepare($sql);
            //$params2 = array('nom' => $result, 'yeari' => intval($cinco), 'hoy' => intval($now2));
            //$stmt1->execute($params2);
            $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

            

        return $this->render('comproprovee/general.html.twig', [
            'unos' => $uno,
            // 'controller_name' => 'ResumenController',
        ]);
    }

    /**
     * @Route("/cuenta/{id}", name="comprove_individual", methods={"GET","POST"})
     */
    public function indexseleccionindividual($id)
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $sql = "SELECT
                comproprovee.id,
                    tipocomp.tcmp_nshort AS TIPO,
                    comproprovee.comp_pdvynumero AS NUMERO,
                    comproprovee.comp_fecha AS FECHA,
                    proveedor_full.pers_nombre AS PROVEEDOR,
                    
                
                    IF (
                           comproprovee.tcmp_id in( 33,25),
                                    (ROUND(comproprovee.comp_saldo, 2)),
                            (ROUND(0,2))
                    ) AS DEBE
                    ,

                    IF (
                     comproprovee.tcmp_id in( 31),
                            (ROUND(comproprovee.comp_saldo, 2)),
                            (ROUND(0,2))
                           
                    ) AS HABER
                    ,

                    IF(comproprovee.comp_apro=0, 'NO', 'SI') AS APROPIADO,
                     comproprovee.comp_total AS TOTAL
                    FROM

                comproprovee 
                INNER JOIN proveedor_full ON proveedor_full.id = comproprovee.prove_id
                INNER JOIN tipocomp ON tipocomp.id = comproprovee.tcmp_id
                WHERE
                comproprovee.prove_id = :idem

                AND
                comproprovee.comp_ctacte = 1
                AND
                comproprovee.comp_apro = 0
                ORDER BY comproprovee.comp_fecha
            
            
            ";
// comprobante.comp_fecha BETWEEN "2019-08-15" AND "2019-08-30" AND
            $stmt1 = $conn->prepare($sql);
            $params2 = array('idem' => $id );
            $stmt1->execute($params2);
            //$stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

            $persona = $this->getDoctrine()
            ->getRepository(Proveedor2::class)
             ->find($id);
        return $this->render('comproprovee/individual.html.twig', [
            'unos' => $uno,
            'persona' => $persona,
        ]);
    }


     /**
     * @Route("/cuenta{id}/all", name="comprove_ind_all", methods={"GET","POST"})
     */
    public function indexseleccionindiall($id)
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();//ya esta acomodaddo
       
        $sql = "SELECT
                comproprovee.id,
                    tipocomp.tcmp_nshort AS TIPO,
                    comproprovee.comp_pdvynumero AS NUMERO,
                    comproprovee.comp_fecha AS FECHA,
                    proveedor_full.pers_nombre AS PROVEEDOR,
                    
                
                    IF (
                          comproprovee.tcmp_id in( 33,25),
                                    (ROUND(comproprovee.comp_saldo, 2)),
                            (ROUND(0,2))
                    ) AS DEBE
                    ,

                    IF (
                     comproprovee.tcmp_id in( 31),
                            (ROUND(comproprovee.comp_saldo, 2)),
                            (ROUND(0,2))
                           
                    ) AS HABER
                    ,

                    IF(comproprovee.comp_apro=0, 'NO', 'SI') AS APROPIADO,
                     comproprovee.comp_total AS TOTAL
                    FROM

                comproprovee 
                INNER JOIN proveedor_full ON proveedor_full.id = comproprovee.prove_id
                INNER JOIN tipocomp ON tipocomp.id = comproprovee.tcmp_id
                WHERE
                comproprovee.prove_id = :idem

                AND
                comproprovee.comp_ctacte = 1
                
                ORDER BY comproprovee.comp_fecha
            
            ";

            $stmt1 = $conn->prepare($sql);
            $params2 = array('idem' => $id );
            $stmt1->execute($params2);
            //$stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

            $persona = $this->getDoctrine()
            ->getRepository(Proveedor2::class)
             ->find($id);
        return $this->render('comproprovee/individual.html.twig', [
            'unos' => $uno,
            'persona' => $persona,
        ]);
    }


    /**
     *
     * @Route("/recibo/{id}/cuenta", name="comprove_cuenta", methods={"GET"})
     * 
     */
    public function indexAction222(Request $request,$id)//ya esta acomodaddo
    {
        $em = $this->getDoctrine()->getManager();
        // $articulos = $em->getRepository('App:Articulo')->findAll();
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $persona = $em->getRepository('App:Proveedor2')->find($id);

        $sql = "SELECT
                comproprovee.id,
                    tipocomp.tcmp_nshort AS TIPO,
                    comproprovee.comp_pdvynumero AS NUMERO,
                    comproprovee.comp_fecha AS FECHA,
                    proveedor_full.pers_nombre AS PROVEEDOR,
                    
                IF (
                    comproprovee.tcmp_id in( 31),
                    (ROUND(comproprovee.comp_total, 2)),
                        (ROUND(comproprovee.comp_total * (-1), 2))
                    
                
                ) AS TOTAL
                ,


                IF (
                    comproprovee.tcmp_id in( 31),
                    
                    (ROUND(comproprovee.comp_saldo, 2)),
                    (ROUND(comproprovee.comp_saldo * (-1), 2))
                    
                ) AS SALDO
                ,


                IF(comproprovee.comp_apro=0, 'NO', 'SI') AS APROPIADO
                FROM
                comproprovee 
                INNER JOIN proveedor_full ON proveedor_full.id = comproprovee.prove_id
                INNER JOIN tipocomp ON tipocomp.id = comproprovee.tcmp_id
                WHERE
                comproprovee.prove_id = :idem 
                AND 
                comproprovee.comp_ctacte = 1
                AND
                comproprovee.comp_apro = 0
                ORDER BY comproprovee.comp_fecha  ";
            $stmt1 = $conn->prepare($sql);
             $params2 = array('idem' => $id);//:idem 
            $stmt1->execute($params2);
            // $stmt1->execute();
            // returns an array of arrays (i.e. a raw data set)
            $uno = $stmt1->fetchAll();

        return $this->render('comproprovee/new33.html.twig', array(
            //            'pedidosweb' => $pedidosweb,
            //            'form' => $form->createView(),
            'persona' => $persona,
            'unos' => $uno,
            // 'articulos' => $articulos,

        ));
    }

    /**
     * 
     * @Route("/cuenta/rec/2", name="comprove_recibo", methods={"GET","POST"})
     */
    public function nuevorecibo2webAction(Request $request)//Recibo 
    {
        $em = $this->getDoctrine()->getManager();
        $idemclie = $request->request->get("idclie");
        // $idemclie =1380;
        //        $idEmpl = 2;
        // $empl_id = $this->getUser()->getId();
        $ctotali = $request->request->get("ctotal");
        $totaluno =number_format( $ctotali,1 );
        $compfech = $request->request->get("cfecha");
        $observ = $request->request->get("observ");
        $largo = $request->request->get("largo");
        $cventa = $request->request->get("cventa");
        $concept = $request->request->get("concept");
        // $cenvio = $request->request->get("cenvio");
        $datos = json_decode($_POST['array'], true);
        // $empleado = $em->getRepository('AppBundle:Empleado')->find( $empl_id);
        $proveedor = $em->getRepository('App:Proveedor2')->find($idemclie);
        //$tipocomp = $em->getRepository('App:Tipocomp')->find(31);//compr nota de pedido
        $pedido = new Comproprovee();
        // $deta = new DetacompRec();
        $mensajes = ' Comprobantes Asoc:';
        $status = array('id' => '','caja'=>0);
        $last_Id2 =0;
        date_default_timezone_set('America/Argentina/Cordoba');
        //        ($idemclie != '') and
        $now = date("Y-m-d H:i:s");
        // SELECT MAX(`comp_numero`)+1 As numero FROM `comprobante` WHERE `tcmp_id`= 31

        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = "SELECT MAX(comp_pdvynumero +1) As numero FROM comproprovee WHERE tcmp_id= 25 "; //31 np edido 32 recibo 33 nota de credito de pedidos //25 oreden de pago proveedor 
        $stmt1 = $conn->prepare($sql);
        $stmt1->execute();
        $ultimo = $stmt1->fetchAll();

        $numero = strval($ultimo[0]['numero']);
        if ($numero == '' ) {
           $numero = '1';
        }

       $tipocomp = $em->getRepository('App:Tipocomp')->find(25);
            ///---------------- totales a cancelar 
        $tot_a_cancel = 0.00;
        $saldo =0.00;
        // $saldo = $ctotali - $tot_a_cancel;//ver aca porque si es negativo el total eje recibo ynota de creditos no apropiadas 

        for ($i = 0; $i <= $largo; $i++) {

              $tot_a_cancel += floatval($datos[$i][5]);
                // $deta->setImportehitorico(floatval($datos[$i][3])); 
        }
        $tot_a_canceluno = number_format( $tot_a_cancel,1 );
        $accion = 0;// 1 apropiado 2 sobra saldo 
        $resto = number_format(  $ctotali - $tot_a_cancel  ,1 ); //aca lo restamos y lo pasomos a un decimal al resultado para comprobar
        if ($totaluno  < 0.0) //en proveedor yo estoydebiendo a proveedor 
        {
            // $last_Id2 =0;
        
           $saldo = $ctotali * (-1);
           //----------------------
            $pedido->setLogiId(1);
            $pedido->setProveedor($proveedor);
            $pedido->setObservaciones($observ);
            $pedido->setTipoComprobante($tipocomp);//
            $pedido->setFecha($compfech);
            $pedido->setFechaVenc($compfech);
            $pedido->setCtacte($cventa);
            $pedido->setCompFechacarga($now);
            $pedido->setPdvynumero($numero);
            $pedido->setTotal(floatval($saldo)); 
            $pedido->setSaldo(floatval($saldo));
            $pedido->setApropiado(0);
            $em->persist($pedido);
            $em->flush();
            $last_Id = $pedido->getId();
           
       
        }elseif ($totaluno >= 0.0) 
        {
          
            //--------------
            if ($resto == 0.0) 
             {//apropiado saldo cero


           //$saldo = $ctotali * (-1);
           //----------------------
            $pedido->setLogiId(1);
            $pedido->setProveedor($proveedor);
            $pedido->setObservaciones($observ);
            $pedido->setTipoComprobante($tipocomp);//
            $pedido->setFecha($compfech);
            $pedido->setFechaVenc($compfech);
            $pedido->setCtacte($cventa);
            $pedido->setCompFechacarga($now);
            $pedido->setPdvynumero($numero);
            $pedido->setTotal(floatval($ctotali)); 
            $pedido->setSaldo(floatval(0));//salda y apropia saldo cero
            $pedido->setApropiado(1);
            $em->persist($pedido);
            $em->flush();
            $last_Id = $pedido->getId();
            $pediddd = $em->getRepository('App:Comproprovee')->find($last_Id);
            if ($last_Id != '') {

              

                for ($i = 0; $i <= $largo; $i++) {
                   
                    $qb = $em->createQueryBuilder();
                    $q = $qb->update('App:Comproprovee', 'c')
                            ->set('c.apropiado', '?1')
                            // ->set('c.compSaldo', '?2')
                            ->where('c.id = ?2')
                            ->setParameter(1, 1)
                            // ->setParameter(2, 0)
                            ->setParameter(2, $datos[$i][0])
                            ->getQuery();
                    $p = $q->execute();
                    $mensajes =$mensajes. $datos[$i][1].' - '.$datos[$i][2].' , ' ;

                    }
                    $qb2 = $em->createQueryBuilder();
                    $q2 = $qb2->update('App:Comproprovee', 'c')
                            ->set('c.observaciones', '?1')
                            // ->set('c.compSaldo', '?2')
                            ->where('c.id = ?2')
                            ->setParameter(1, $observ . ' '.$mensajes)
                            // ->setParameter(2, 0)
                            ->setParameter(2, $last_Id)
                            ->getQuery();
                    $p2 = $q2->execute();
                }


            

            }elseif ($resto > 0.0 ) { //queda dinero a favor de nosotros saldamos el proveedor
               
            $saldo = $resto ;//* (-1);
           //----------------------
            $pedido->setLogiId(1);
            $pedido->setProveedor($proveedor);
            $pedido->setObservaciones($observ);
            $pedido->setTipoComprobante($tipocomp);//
            $pedido->setFecha($compfech);
            $pedido->setFechaVenc($compfech);
            $pedido->setCtacte($cventa);
            $pedido->setCompFechacarga($now);
            $pedido->setPdvynumero($numero);
            $pedido->setTotal(floatval($ctotali));
            $pedido->setSaldo(floatval($saldo));
            $pedido->setApropiado(0);
            $em->persist($pedido);
            $em->flush();
            $last_Id = $pedido->getId();
            $pediddd = $em->getRepository('App:Comproprovee')->find($last_Id);
            if ($last_Id != '') {

              

                for ($i = 0; $i <= $largo; $i++) {
                   
                    $qb = $em->createQueryBuilder();
                    $q = $qb->update('App:Comproprovee', 'c')
                            ->set('c.apropiado', '?1')
                            // ->set('c.compSaldo', '?2')
                            ->where('c.id = ?2')
                            ->setParameter(1, 1)
                            // ->setParameter(2, 0)
                            ->setParameter(2, $datos[$i][0])
                            ->getQuery();
                    $p = $q->execute();
                    $mensajes =$mensajes. $datos[$i][1].' - '.$datos[$i][2].' , ' ;

                    }
                    $qb2 = $em->createQueryBuilder();
                    $q2 = $qb2->update('App:Comproprovee', 'c')
                            ->set('c.observaciones', '?1')
                            // ->set('c.compSaldo', '?2')
                            ->where('c.id = ?2')
                            ->setParameter(1, $observ . ' '.$mensajes)
                            // ->setParameter(2, 0)
                            ->setParameter(2, $last_Id)
                            ->getQuery();
                    $p2 = $q2->execute();
                }


            }
           
        
            sleep(3);

            $status = array('id' => $last_Id,'caja'=>$last_Id2);
     
            
        }
        return new JsonResponse($status);
    }

}
