<?php

namespace App\Form;

use App\Entity\DetacompFac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DetacompFacType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prodId')
            ->add('prodDescri')
            ->add('prodPvp')
            ->add('prodPercdto')
            ->add('prodImpdto')
            ->add('prodImpiva')
            ->add('prodCant')
            ->add('comprobante')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DetacompFac::class,
        ]);
    }
}
