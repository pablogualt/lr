<?php

namespace App\Form;

use App\Entity\Articulo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticuloType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('artiCodigoOriginal')
            ->add('artiCodigoAlternativo')
            ->add('artiCodigoPropio')
            ->add('artiCbarra')
            ->add('rubrId')
            ->add('srubId')
            ->add('artiNombre')
            ->add('artiDescri')
            ->add('unidId')
            ->add('artiStock')
            ->add('artiStockmin')
            ->add('artiPvp')
            ->add('artiFechapvp')
            ->add('artiPreciocosto')
            ->add('artiFechapcosto')
            ->add('proveeId')
            ->add('aivaId')
            ->add('monedaId')
            ->add('artiUbicacion')
            ->add('artiObservaciones')
            ->add('artiImagenblob')
            ->add('artiMargen')
            ->add('marca')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Articulo::class,
        ]);
    }
}
