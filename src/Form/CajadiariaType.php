<?php

namespace App\Form;

use App\Entity\Cajadiaria;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CajadiariaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('comprobante')
            // ->add('descripcion')
            // ->add('fecha')
            ->add('debe')
            ->add('haber')
            // ->add('persona')
            // ->add('puntoDeVenta')
            // ->add('tipoComprobante')
            // ->add('user')
            // ->add('compTimestamp')
            ->add('observaciones', TextareaType::class, array(
                'attr' => array('class' => 'tinymce'), 'required'    => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cajadiaria::class,
        ]);
    }
}
