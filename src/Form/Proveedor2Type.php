<?php

namespace App\Form;

use App\Entity\Proveedor2;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Proveedor2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cuit')
            ->add('documento')
            ->add('pjuridica')
            ->add('nrolegajo')
            ->add('direccion')
            ->add('nombre')
            ->add('nombFantasia')
            ->add('ibrutos')
            ->add('telFax')
            ->add('celular')
            ->add('email')
            ->add('contacto')
            ->add('celContacto')
            ->add('observacion')
            ->add('zona')
            ->add('gremio')
            ->add('ctacte')
            ->add('tipoDoc')
            ->add('condicionDeIva')
            ->add('localidad')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Proveedor2::class,
        ]);
    }
}
