<?php

namespace App\Form;

use App\Entity\Comproprovee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ComproproveeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('mepaId')
             ->add('tipoComprobante')
            ->add('proveedor')
            ->add('fecha')
            ->add('ctacte')
            ->add('pdvynumero')
            // ->add('compLetra')
            ->add('subtotal')
            // ->add('descPctje')
            // ->add('descuento')
            // ->add('neto')
            // ->add('perceiva')
            // ->add('perceibrut')
            // ->add('otroimpuesto')
            // ->add('aivaId')
            // ->add('compTotiva')
            ->add('total')
            ->add('observaciones', TextareaType::class, array(
                'attr' => array('class' => 'tinymce'), 'required'    => false
            ))
            // ->add('apropiado')
            // ->add('logiId')
            // ->add('saldo')
            // ->add('fechaVenc')
            ->add('compFechacarga')
            // ->add('mesImpfiscal')
            // ->add('anioImpfiscal')
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comproprovee::class,
        ]);
    }
}
