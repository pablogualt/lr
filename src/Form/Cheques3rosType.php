<?php

namespace App\Form;

use App\Entity\Cheques3ros;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class Cheques3rosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero')
            ->add('titular', null, array('label' => 'Titular del Cheque'))
            ->add('cuitDelTitular')
            ->add('importe')
            ->add('banco')
            ->add('localidad')
            ->add('fechaDeCobro')
            ->add('fechaDeEmision')
            ->add('fechaDeRecibido')
           
            // ->add('fechaDeSalida')
            // ->add('persona')
            ->add('personaDescripcion', null, array('label' => 'Persona de Quien Lo Recibo'))
            ->add('estado')
             ->add('destino')
            ->add('observaciones', TextareaType::class, array(
                'attr' => array('class' => 'tinymce'), 'required'    => false
            ))
           
           
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cheques3ros::class,
        ]);
    }
}


