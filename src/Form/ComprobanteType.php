<?php

namespace App\Form;

use App\Entity\Comprobante;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ComprobanteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('compFecha', null, array('label' => 'FECHA'))
            ->add('cliente')
            ->add('compCtacte')
            ->add('tcmpId')
            // ->add('compPdventa')
            ->add('compNumero', null, array('label' => 'Numero'))
            // ->add('compSubtot1')
            // ->add('compDescPctje')
            // ->add('compDescuento')
            // ->add('compImpuest')
            // ->add('compSubtot2')
            // ->add('aivaId')
            // ->add('compTotiva')
            ->add('compTotal', null, array('label' => 'TOTAL'))
            // ->add('compConcepto')
            ->add('observaciones', TextareaType::class, array(
                'attr' => array('class' => 'tinymce'), 'required'    => false
            ))

            // ->add('moneId')
            // ->add('compCambio')
            ->add('logiId', null, array('label' => false));
            // ->add('compApro')
            // ->add('compSaldo')
            // ->add('compFechaVenc')
            //  ->add('mepaId')
            // ->add('compCae')
            // ->add('compFvcae')
            // ->add('compAliciva')
            // ->add('compCbarra')
            // ->add('compTimestamp')
            // ->add('compPeriododesde')
            // ->add('compPeriodohasta')
            // ->add('compPeriodovtopago')
            // ->add('compNogravado')
            // ->add('compExento')
            // ->add('compPerciva')

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comprobante::class,
        ]);
    }
}
