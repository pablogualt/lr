<?php

namespace App\Form;

use App\Entity\DetacompRec;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DetacompRecType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('decoConcepto')
            ->add('cbtasocId')
            ->add('importe')
            ->add('decoObse')
            ->add('comprobante')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DetacompRec::class,
        ]);
    }
}
