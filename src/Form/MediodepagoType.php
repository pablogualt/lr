<?php

namespace App\Form;

use App\Entity\Mediodepago;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MediodepagoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('activo')
            // ->add('cuentaId')
            // ->add('tarjetaDeCredito')
            // ->add('otros')
            // ->add('descuento')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mediodepago::class,
        ]);
    }
}
