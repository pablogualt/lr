<?php

namespace App\Form;

use App\Entity\Persona;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PersonaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre')->add('nombFantasia')->add('cuit')->add('documento')->add('pjuridica')->add('nrolegajo')->add('direccion')
            ->add('ibrutos')->add('telFax')->add('celular')
            ->add('email')->add('contacto')->add('celContacto')->add('observacion')->add('tipoDoc')
            ->add('condicionDeIva')->add('localidad', null, array('label' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Persona::class,
        ]);
    }
}
