<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Localidad
 *
 * @ORM\Table(name="localidad", indexes={@ORM\Index(name="cp", columns={"loca_cpostal"})})
 * @ORM\Entity
 */
class Localidad
{
    /**
     * @var int
     *
     * @ORM\Column(name="loca_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $locaId;

    /**
     * @var string
     *
     * @ORM\Column(name="loca_nombre", type="text", length=255, nullable=false)
     */
    private $localidad;

    /**
     * @var int
     *
     * @ORM\Column(name="prov_id", type="integer", nullable=false)
     */
    private $provId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Provincia")
     * @ORM\JoinColumn(name="prov_id", referencedColumnName="prov_id")
     */
    private $provincia;

    /**
     * @var int
     *
     * @ORM\Column(name="loca_cpostal", type="integer", nullable=false)
     */
    private $codigoPostal;

    /**
     * @return int
     */
    public function getLocaId()
    {
        return $this->locaId;
    }

    /**
     * @return string
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * @return int
     */
    public function getProvId()
    {
        return $this->provId;
    }

    /**
     * @return mixed
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * @return int
     */
    public function getCodigoPostal()
    {
        return $this->codigoPostal;
    }



    public function __toString()
    {
        return (string) $this->localidad.' - '.$this->provincia.' - '.$this->codigoPostal;
    }


}
