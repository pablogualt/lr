<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cambio
 *
 * @ORM\Table(name="cambio")
 * @ORM\Entity
 */
class Cambio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="mone_id", type="integer", nullable=false)
     */
    private $moneda;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="camb_fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var float|null
     *
     * @ORM\Column(name="camb_venta", type="float", precision=8, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $venta = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="camb_compra", type="float", precision=8, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $compra = '0.00';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="camb_actualizado", type="boolean", nullable=true, options={"comment"="0 es sin actaulizar merca 1 se ctualizo la merca ese dia "})
     */
    private $actualizado = '0';


}
