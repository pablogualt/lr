<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Comproprovee
 *
 * @ORM\Table(name="comproprovee")
 * @ORM\Entity
 */
class Comproprovee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mepa_id", type="integer", nullable=true)
     */
    private $mepaId= 0;


    

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tipocomp")
     * @ORM\JoinColumn(name="tcmp_id", referencedColumnName="id")
     */
    private $tipoComprobante;

    
     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proveedor2")
     * @ORM\JoinColumn(name="prove_id", referencedColumnName="id")
     */
    private $proveedor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_fecha", type="string", nullable=false)
     */
    private $fecha;

    /**
     * @var bool
     *
     * @ORM\Column(name="comp_ctacte", type="boolean", nullable=false, options={"default"="1","comment"="defecto true no contado"})
     */
    private $ctacte = true;

    /**
     * @var string
     *
     * @ORM\Column(name="comp_pdvynumero", type="text", length=255, nullable=false)
     */
    private $pdvynumero= '02';


    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_letra", type="string", length=1, nullable=false)
     */
    private $compLetra= '';


    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_subtotal", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $subtotal = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_desc_pctje", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00","comment"="Lo utilizo para NO GRAVADO"})
     */
    private $descPctje = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_descuento", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00","comment"="Lo utilizo para EXENTO"})
     */
    private $descuento = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_netogravado", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $neto = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_perceiva", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00","comment"="Redundante con Otros Tributos - Lo utilizo para tirar el IVA Compras"})
     */
    private $perceiva = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_perceibrut", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $perceibrut = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_otroimpuesto", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $otroimpuesto = '0.00';

    /**
     * @var int
     *
     * @ORM\Column(name="aiva_id", type="integer", nullable=false, options={"default"="5"})
     */
    private $aivaId = '5';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_totiva", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $compTotiva = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="comp_total", type="float", precision=11, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $total = '0.00';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_obse", type="text", length=255, nullable=true)
     */
    private $observaciones= '0';


    /**
     * @var bool
     *
     * @ORM\Column(name="comp_apro", type="boolean", nullable=false, options={"comment"="Default false no apropiado"})
     */
    private $apropiado = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="logi_id", type="integer", nullable=true)
     */
    private $logiId = '0';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_saldo", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $saldo = '0.00';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_fecha_venc", type="string", nullable=true)
     */
    private $fechaVenc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_fechacarga", type="string", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $compFechacarga = '';

    /**
     * @var int
     *
     * @ORM\Column(name="mes_impfiscal", type="integer", nullable=false)
     */
    private $mesImpfiscal = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="anio_impfiscal", type="integer", nullable=false)
     */
    private $anioImpfiscal = '0';


     public function __toString()
    {
        return (string) $this->fecha . ' - ' . $this->proveedor . ' - ' . $this->total;
    }

    



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMepaId()
    {
        return $this->mepaId;
    }

    /**
     * @param int|null $mepaId
     *
     * @return self
     */
    public function setMepaId($mepaId)
    {
        $this->mepaId = $mepaId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTipoComprobante()
    {
        return $this->tipoComprobante;
    }

    /**
     * @param mixed $tipoComprobante
     *
     * @return self
     */
    public function setTipoComprobante($tipoComprobante)
    {
        $this->tipoComprobante = $tipoComprobante;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * @param mixed $proveedor
     *
     * @return self
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param string|null $fecha
     *
     * @return self
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCtacte()
    {
        return $this->ctacte;
    }

    /**
     * @param bool $ctacte
     *
     * @return self
     */
    public function setCtacte($ctacte)
    {
        $this->ctacte = $ctacte;

        return $this;
    }

    /**
     * @return string
     */
    public function getPdvynumero()
    {
        return $this->pdvynumero;
    }

    /**
     * @param string $pdvynumero
     *
     * @return self
     */
    public function setPdvynumero($pdvynumero)
    {
        $this->pdvynumero = $pdvynumero;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCompLetra()
    {
        return $this->compLetra;
    }

    /**
     * @param string|null $compLetra
     *
     * @return self
     */
    public function setCompLetra($compLetra)
    {
        $this->compLetra = $compLetra;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param float|null $subtotal
     *
     * @return self
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getDescPctje()
    {
        return $this->descPctje;
    }

    /**
     * @param float|null $descPctje
     *
     * @return self
     */
    public function setDescPctje($descPctje)
    {
        $this->descPctje = $descPctje;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * @param float|null $descuento
     *
     * @return self
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getNeto()
    {
        return $this->neto;
    }

    /**
     * @param float|null $neto
     *
     * @return self
     */
    public function setNeto($neto)
    {
        $this->neto = $neto;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPerceiva()
    {
        return $this->perceiva;
    }

    /**
     * @param float|null $perceiva
     *
     * @return self
     */
    public function setPerceiva($perceiva)
    {
        $this->perceiva = $perceiva;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPerceibrut()
    {
        return $this->perceibrut;
    }

    /**
     * @param float|null $perceibrut
     *
     * @return self
     */
    public function setPerceibrut($perceibrut)
    {
        $this->perceibrut = $perceibrut;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getOtroimpuesto()
    {
        return $this->otroimpuesto;
    }

    /**
     * @param float|null $otroimpuesto
     *
     * @return self
     */
    public function setOtroimpuesto($otroimpuesto)
    {
        $this->otroimpuesto = $otroimpuesto;

        return $this;
    }

    /**
     * @return int
     */
    public function getAivaId()
    {
        return $this->aivaId;
    }

    /**
     * @param int $aivaId
     *
     * @return self
     */
    public function setAivaId($aivaId)
    {
        $this->aivaId = $aivaId;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getCompTotiva()
    {
        return $this->compTotiva;
    }

    /**
     * @param float|null $compTotiva
     *
     * @return self
     */
    public function setCompTotiva($compTotiva)
    {
        $this->compTotiva = $compTotiva;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     *
     * @return self
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * @param string|null $observaciones
     *
     * @return self
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * @return bool
     */
    public function isApropiado()
    {
        return $this->apropiado;
    }

    /**
     * @param bool $apropiado
     *
     * @return self
     */
    public function setApropiado($apropiado)
    {
        $this->apropiado = $apropiado;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLogiId()
    {
        return $this->logiId;
    }

    /**
     * @param int|null $logiId
     *
     * @return self
     */
    public function setLogiId($logiId)
    {
        $this->logiId = $logiId;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param float|null $saldo
     *
     * @return self
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFechaVenc()
    {
        return $this->fechaVenc;
    }

    /**
     * @param string|null $fechaVenc
     *
     * @return self
     */
    public function setFechaVenc($fechaVenc)
    {
        $this->fechaVenc = $fechaVenc;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCompFechacarga()
    {
        return $this->compFechacarga;
    }

    /**
     * @param string|null $compFechacarga
     *
     * @return self
     */
    public function setCompFechacarga($compFechacarga)
    {
        $this->compFechacarga = $compFechacarga;

        return $this;
    }

    /**
     * @return int
     */
    public function getMesImpfiscal()
    {
        return $this->mesImpfiscal;
    }

    /**
     * @param int $mesImpfiscal
     *
     * @return self
     */
    public function setMesImpfiscal($mesImpfiscal)
    {
        $this->mesImpfiscal = $mesImpfiscal;

        return $this;
    }

    /**
     * @return int
     */
    public function getAnioImpfiscal()
    {
        return $this->anioImpfiscal;
    }

    /**
     * @param int $anioImpfiscal
     *
     * @return self
     */
    public function setAnioImpfiscal($anioImpfiscal)
    {
        $this->anioImpfiscal = $anioImpfiscal;

        return $this;
    }
}
