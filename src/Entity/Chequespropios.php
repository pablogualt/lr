<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Chequespropios
 *
 * @ORM\Table(name="chequespropios")
 * @ORM\Entity
 */
class Chequespropios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="bncu_id", type="integer", nullable=false, options={"comment"="banco_cuentas cuenta tiene el id de banco"})
     */
    private $identCuentaBanco = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="chqra_id", type="integer", nullable=true, options={"comment"="sin uso"})
     */
    private $chequeraNumero = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="chpr_numero", type="string", length=8, nullable=false)
     */
    private $numero = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="chpr_fechpago", type="date", nullable=false)
     */
    private $fechaDePago;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="chpr_fechemi", type="date", nullable=false)
     */
    private $fechaEmision;

    /**
     * @var float
     *
     * @ORM\Column(name="chpr_importe", type="float", precision=11, scale=0, nullable=false)
     */
    private $importe = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="orpa_id", type="integer", nullable=true, options={"comment"="redundante?"})
     */
    private $orpaId = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="chpr_obseva", type="string", length=255, nullable=true)
     */
    private $obsevacion = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="cheq_destino", type="integer", nullable=true)
     */
    private $chequeDestino = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="pers_id", type="integer", nullable=true)
     */
    private $persona = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="chpr_timestamp", type="datetime", nullable=true)
     */
    private $chprTimestamp;


}
