<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Civa
 *
 * @ORM\Table(name="civa")
 * @ORM\Entity
 */
class Civa
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="civa_codigo", type="text", length=255, nullable=true)
     */
    private $civaCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="civa_desc", type="text", length=255, nullable=false)
     */
    private $civaDesc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="civa_short", type="text", length=255, nullable=true)
     */
    private $civaShort;

    /**
     * @var string
     *
     * @ORM\Column(name="civa_letra", type="text", length=255, nullable=false)
     */
    private $civaLetra;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getCivaCodigo()
    {
        return $this->civaCodigo;
    }

    /**
     * @return string
     */
    public function getCivaDesc()
    {
        return $this->civaDesc;
    }

    /**
     * @return null|string
     */
    public function getCivaShort()
    {
        return $this->civaShort;
    }

    /**
     * @return string
     */
    public function getCivaLetra()
    {
        return $this->civaLetra;
    }


    public function __toString()
    {
        return (string) $this->civaDesc.' - '.$this->civaLetra;
    }

}
