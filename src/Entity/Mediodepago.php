<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mediodepago
 *
 * @ORM\Table(name="mediodepago")
 * @ORM\Entity
 */
class Mediodepago
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mepa_desc", type="string", length=100, nullable=false)
     */
    private $nombre = '';

    /**
     * @var bool
     *
     * @ORM\Column(name="mepa_activo", type="boolean", nullable=false, options={"default"="1","comment"="1- true"})
     */
    private $activo = '1';

    /**
     * @var int|null
     *
     * @ORM\Column(name="cuenta_id", type="integer", nullable=true, options={"comment"="cuenta asociada del plan de cuentas"})
     */
    private $cuentaId = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="mepa_tcred", type="boolean", nullable=true, options={"comment"="tarj de cred por defecto 0 - no es tcrd "})
     */
    private $tarjetaDeCredito = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="mepa_otros", type="boolean", nullable=true, options={"comment"="entre en otros - 0 no es otros"})
     */
    private $otros = '0';

    /**
     * @var float|null
     *
     * @ORM\Column(name="mepa_descuento", type="float", precision=6, scale=2, nullable=true, options={"default"="0.00","comment"="lo que te retiene"})
     */
    private $descuento = '0.00';
    

    public function __toString()
    {
        return (string) $this->nombre.'-'.($this->activo ? 'Activo' : 'No');
    }



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActivo()
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     *
     * @return self
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCuentaId()
    {
        return $this->cuentaId;
    }

    /**
     * @param int|null $cuentaId
     *
     * @return self
     */
    public function setCuentaId($cuentaId)
    {
        $this->cuentaId = $cuentaId;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isTarjetaDeCredito()
    {
        return $this->tarjetaDeCredito;
    }

    /**
     * @param bool|null $tarjetaDeCredito
     *
     * @return self
     */
    public function setTarjetaDeCredito($tarjetaDeCredito)
    {
        $this->tarjetaDeCredito = $tarjetaDeCredito;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isOtros()
    {
        return $this->otros;
    }

    /**
     * @param bool|null $otros
     *
     * @return self
     */
    public function setOtros($otros)
    {
        $this->otros = $otros;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * @param float|null $descuento
     *
     * @return self
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;

        return $this;
    }
}
