<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rubro
 *
 * @ORM\Table(name="rubro")
 * @ORM\Entity
 */
class Rubro
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rubr_nombre", type="text", length=255, nullable=true, options={"default"="NULL"})
     */
    private $rubrNombre = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="rubr_codigo", type="integer", nullable=true, options={"default"="1"})
     */
    private $rubrCodigo = '1';

    public function __toString()
    {
        return $this->rubrNombre;
    }


    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of rubrNombre
     *
     * @return  string|null
     */ 
    public function getRubrNombre()
    {
        return $this->rubrNombre;
    }

    /**
     * Get the value of rubrCodigo
     *
     * @return  int|null
     */ 
    public function getRubrCodigo()
    {
        return $this->rubrCodigo;
    }

}
