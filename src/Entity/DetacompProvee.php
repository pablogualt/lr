<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * DetacompProvee
 *
 * @ORM\Table(name="detacomp_provee")
 * @ORM\Entity
 */
class DetacompProvee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="comp_id", type="integer", nullable=false)
     */
    private $compId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="prod_id", type="integer", nullable=true)
     */
    private $prodId;

    /**
     * @var string
     *
     * @ORM\Column(name="prod_descri", type="string", length=250, nullable=false)
     */
    private $prodDescri;

    /**
     * @var float
     *
     * @ORM\Column(name="prod_precio", type="float", precision=10, scale=0, nullable=false)
     */
    private $prodPrecio = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="prod_iva", type="float", precision=10, scale=0, nullable=false)
     */
    private $prodIva = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="prod_cant", type="float", precision=10, scale=0, nullable=false)
     */
    private $prodCant = '0';


}
