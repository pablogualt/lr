<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tdoc
 *
 * @ORM\Table(name="tdoc")
 * @ORM\Entity
 */
class Tdoc
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tdoc_codigo", type="text", length=255, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="tdoc_desc", type="text", length=255, nullable=false)
     */
    private $descripcion;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }


    public function __toString()
    {
        return (string) $this->codigo.' - '.$this->descripcion;
    }


}
