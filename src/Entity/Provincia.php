<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Provincia
 *
 * @ORM\Table(name="provincia")
 * @ORM\Entity
 */
class Provincia
{
    /**
     * @var int
     *
     * @ORM\Column(name="prov_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $provId;

    /**
     * @var string
     *
     * @ORM\Column(name="prov_nombre", type="text", length=255, nullable=false)
     */
    private $provincia;

    /**
     * @return int
     */
    public function getProvId()
    {
        return $this->provId;
    }

    /**
     * @return string
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    public function __toString()
    {
        return (string) $this->provincia;
    }
}
