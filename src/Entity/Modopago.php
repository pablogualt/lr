<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Modopago
 *
 * @ORM\Table(name="modopago")
 * @ORM\Entity
 */
class Modopago
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="modp_descr", type="text", length=255, nullable=false)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="modp_short", type="text", length=255, nullable=false)
     */
    private $descripcionCorta;


}
