<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipocomp
 *
 * @ORM\Table(name="tipocomp")
 * @ORM\Entity
 */
class Tipocomp
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tcmp_codigo", type="text", length=255, nullable=false)
     */
    private $tcmpCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="tcmp_denomina", type="text", length=255, nullable=false)
     */
    private $tcmpDenomina;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tcmp_nshort", type="text", length=255, nullable=true, options={"default"="NULL"})
     */
    private $tcmpNshort = 'NULL';



    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of tcmpCodigo
     *
     * @return  string
     */ 
    public function getTcmpCodigo()
    {
        return $this->tcmpCodigo;
    }

    /**
     * Get the value of tcmpDenomina
     *
     * @return  string
     */ 
    public function getTcmpDenomina()
    {
        return $this->tcmpDenomina;
    }

    /**
     * Get the value of tcmpNshort
     *
     * @return  string|null
     */ 
    public function getTcmpNshort()
    {
        return $this->tcmpNshort;
    }

    public function __toString()
    {
        return $this->tcmpCodigo.' - '.$this->tcmpDenomina;
    }
}
