<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Asiedeta
 *
 * @ORM\Table(name="asiedeta")
 * @ORM\Entity
 */
class Asiedeta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="asie_id", type="integer", nullable=true)
     */
    private $asieId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="asdt_cuenta", type="integer", nullable=true)
     */
    private $asdtCuenta;

    /**
     * @var float|null
     *
     * @ORM\Column(name="asdt_cuedeimpo", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $asdtCuedeimpo = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="asdt_cuehaimpo", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $asdtCuehaimpo = '0.00';


}
