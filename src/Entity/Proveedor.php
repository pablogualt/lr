<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proveedor
 *
 * @ORM\Table(name="proveedor")
 * @ORM\Entity
 */
class Proveedor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var float|null
     *
     * @ORM\Column(name="prove_flet", type="float", precision=10, scale=0, nullable=true)
     */
    private $proveFlet = '0';

    /**
     * @var float|null
     *
     * @ORM\Column(name="prove_descuento", type="float", precision=10, scale=0, nullable=true)
     */
    private $proveDescuento = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="prove_condicion", type="string", length=250, nullable=true)
     */
    private $proveCondicion = '';






   

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getProveFlet()
    {
        return $this->proveFlet;
    }

    /**
     * @param float|null $proveFlet
     *
     * @return self
     */
    public function setProveFlet($proveFlet)
    {
        $this->proveFlet = $proveFlet;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getProveDescuento()
    {
        return $this->proveDescuento;
    }

    /**
     * @param float|null $proveDescuento
     *
     * @return self
     */
    public function setProveDescuento($proveDescuento)
    {
        $this->proveDescuento = $proveDescuento;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProveCondicion()
    {
        return $this->proveCondicion;
    }

    /**
     * @param string|null $proveCondicion
     *
     * @return self
     */
    public function setProveCondicion($proveCondicion)
    {
        $this->proveCondicion = $proveCondicion;

        return $this;
    }


    
}
