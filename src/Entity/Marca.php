<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Marca
 *
 * @ORM\Table(name="marca")
 * @ORM\Entity
 */
class Marca
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="marc_nombre", type="string", length=45, nullable=false)
     */
    private $marcNombre;

    /**
     * @var float|null
     *
     * @ORM\Column(name="marc_bonificacion", type="float", precision=10, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $marcBonificacion = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="marc_margen", type="float", precision=10, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $marcMargen = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="marc_comicion", type="float", precision=10, scale=2, nullable=true, options={"default"="6.00"})
     */
    private $marcComicion = '6.00';



    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of marcNombre
     *
     * @return  string
     */ 
    public function getMarcNombre()
    {
        return $this->marcNombre;
    }

    /**
     * Get the value of marcBonificacion
     *
     * @return  float|null
     */ 
    public function getMarcBonificacion()
    {
        return $this->marcBonificacion;
    }

    /**
     * Get the value of marcMargen
     *
     * @return  float
     */ 
    public function getMarcMargen()
    {
        return $this->marcMargen;
    }

    /**
     * Get the value of marcComicion
     *
     * @return  float|null
     */ 
    public function getMarcComicion()
    {
        return $this->marcComicion;
    }

    public function __toString()
    {
        return $this->marcNombre;
    }
}
