<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * DetacompRec
 *
 * @ORM\Table(name="detacomp_rec")
 * @ORM\Entity
 */
class DetacompRec
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Comprobante", inversedBy="detarecibo")
     * @ORM\JoinColumn(name="comp_id", referencedColumnName="id")
     */
    private $comprobante;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deco_concepto", type="text", length=255, nullable=true, options={"default"="NULL"})
     */
    private $decoConcepto = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="cbtAsoc_id", type="integer", nullable=true, options={"comment"="Comprobante asociado ID, si hay"})
     */
    private $cbtasocId = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="importe", type="float", precision=10, scale=0, nullable=false)
     */
    private $importe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deco_obse", type="text", length=255, nullable=true, options={"default"="NULL"})
     */
    private $decoObse = null;



    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of comprobante
     */ 
    public function getComprobante()
    {
        return $this->comprobante;
    }

    /**
     * Set the value of comprobante
     *
     * @return  self
     */ 
    public function setComprobante($comprobante)
    {
        $this->comprobante = $comprobante;

        return $this;
    }

    /**
     * Get the value of decoConcepto
     *
     * @return  string|null
     */ 
    public function getDecoConcepto()
    {
        return $this->decoConcepto;
    }

    /**
     * Set the value of decoConcepto
     *
     * @param  string|null  $decoConcepto
     *
     * @return  self
     */ 
    public function setDecoConcepto($decoConcepto)
    {
        $this->decoConcepto = $decoConcepto;

        return $this;
    }

    /**
     * Get the value of cbtasocId
     *
     * @return  int|null
     */ 
    public function getCbtasocId()
    {
        return $this->cbtasocId;
    }

    /**
     * Set the value of cbtasocId
     *
     * @param  int|null  $cbtasocId
     *
     * @return  self
     */ 
    public function setCbtasocId($cbtasocId)
    {
        $this->cbtasocId = $cbtasocId;

        return $this;
    }

    /**
     * Get the value of importe
     *
     * @return  float
     */ 
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set the value of importe
     *
     * @param  float  $importe
     *
     * @return  self
     */ 
    public function setImporte(float $importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get the value of decoObse
     *
     * @return  string|null
     */ 
    public function getDecoObse()
    {
        return $this->decoObse;
    }

    /**
     * Set the value of decoObse
     *
     * @param  string|null  $decoObse
     *
     * @return  self
     */ 
    public function setDecoObse($decoObse)
    {
        $this->decoObse = $decoObse;

        return $this;
    }

    public function __toString()
    {
        return $this->decoConcepto. ' - $'.$this->importe;
    }
}
