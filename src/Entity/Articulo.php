<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Articulo
 *
 * @ORM\Table(name="articulo")
 * @ORM\Entity
 */
class Articulo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="arti_codigo_original", type="string", length=100, nullable=false)
     */
    private $artiCodigoOriginal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="arti_codigo_alternativo", type="string", length=100, nullable=true, options={"default"="''"})
     */
    private $artiCodigoAlternativo = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="arti_codigo_propio", type="string", length=100, nullable=true, options={"default"="''"})
     */
    private $artiCodigoPropio = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="arti_cbarra", type="string", length=100, nullable=true, options={"default"="''"})
     */
    private $artiCbarra = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="rubr_id", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $rubrId = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="srub_id", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $srubId = 'NULL';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Marca", inversedBy="articulo")
     * @ORM\JoinColumn(name="marc_id", referencedColumnName="id")
     */
    private $marca;

    /**
     * @var string
     *
     * @ORM\Column(name="arti_nombre", type="string", length=200, nullable=false, options={"default"="''"})
     */
    private $artiNombre = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="arti_descri", type="string", length=250, nullable=true, options={"default"="''"})
     */
    private $artiDescri = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="unid_id", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $unidId = 'NULL';

    /**
     * @var float|null
     *
     * @ORM\Column(name="arti_stock", type="float", precision=8, scale=2, nullable=true, options={"default"="NULL"})
     */
    private $artiStock = 'NULL';

    /**
     * @var float|null
     *
     * @ORM\Column(name="arti_stockMin", type="float", precision=8, scale=2, nullable=true, options={"default"="NULL"})
     */
    private $artiStockmin = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="arti_pvp", type="float", precision=8, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $artiPvp = '0.00';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="arti_fechaPvp", type="date", nullable=true, options={"default"="NULL"})
     */
    private $artiFechapvp = 'NULL';

    /**
     * @var float|null
     *
     * @ORM\Column(name="arti_precioCosto", type="float", precision=8, scale=2, nullable=true, options={"default"="0.00","comment"="defecto cero"})
     */
    private $artiPreciocosto = '0.00';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="arti_fechaPCosto", type="date", nullable=true, options={"default"="NULL"})
     */
    private $artiFechapcosto = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="provee_id", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $proveeId = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="aiva_id", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $aivaId = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="moneda_id", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $monedaId = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="arti_ubicacion", type="string", length=200, nullable=true, options={"default"="NULL"})
     */
    private $artiUbicacion = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="arti_observaciones", type="string", length=255, nullable=true, options={"default"="''"})
     */
    private $artiObservaciones = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="arti_imagenblob", type="blob", length=0, nullable=true, options={"default"="NULL"})
     */
    private $artiImagenblob = 'NULL';

    /**
     * @var float|null
     *
     * @ORM\Column(name="arti_margen", type="float", precision=10, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $artiMargen = '0.00';




    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of artiCodigoOriginal
     *
     * @return  string
     */ 
    public function getArtiCodigoOriginal()
    {
        return $this->artiCodigoOriginal;
    }

    /**
     * Set the value of artiCodigoOriginal
     *
     * @param  string  $artiCodigoOriginal
     *
     * @return  self
     */ 
    public function setArtiCodigoOriginal(string $artiCodigoOriginal)
    {
        $this->artiCodigoOriginal = $artiCodigoOriginal;

        return $this;
    }

    /**
     * Get the value of artiCodigoAlternativo
     *
     * @return  string|null
     */ 
    public function getArtiCodigoAlternativo()
    {
        return $this->artiCodigoAlternativo;
    }

    /**
     * Set the value of artiCodigoAlternativo
     *
     * @param  string|null  $artiCodigoAlternativo
     *
     * @return  self
     */ 
    public function setArtiCodigoAlternativo($artiCodigoAlternativo)
    {
        $this->artiCodigoAlternativo = $artiCodigoAlternativo;

        return $this;
    }

    /**
     * Get the value of artiCodigoPropio
     *
     * @return  string|null
     */ 
    public function getArtiCodigoPropio()
    {
        return $this->artiCodigoPropio;
    }

    /**
     * Set the value of artiCodigoPropio
     *
     * @param  string|null  $artiCodigoPropio
     *
     * @return  self
     */ 
    public function setArtiCodigoPropio($artiCodigoPropio)
    {
        $this->artiCodigoPropio = $artiCodigoPropio;

        return $this;
    }

    /**
     * Get the value of artiCbarra
     *
     * @return  string|null
     */ 
    public function getArtiCbarra()
    {
        return $this->artiCbarra;
    }

    /**
     * Set the value of artiCbarra
     *
     * @param  string|null  $artiCbarra
     *
     * @return  self
     */ 
    public function setArtiCbarra($artiCbarra)
    {
        $this->artiCbarra = $artiCbarra;

        return $this;
    }

    /**
     * Get the value of rubrId
     *
     * @return  int|null
     */ 
    public function getRubrId()
    {
        return $this->rubrId;
    }

    /**
     * Set the value of rubrId
     *
     * @param  int|null  $rubrId
     *
     * @return  self
     */ 
    public function setRubrId($rubrId)
    {
        $this->rubrId = $rubrId;

        return $this;
    }

    /**
     * Get the value of srubId
     *
     * @return  int|null
     */ 
    public function getSrubId()
    {
        return $this->srubId;
    }

    /**
     * Set the value of srubId
     *
     * @param  int|null  $srubId
     *
     * @return  self
     */ 
    public function setSrubId($srubId)
    {
        $this->srubId = $srubId;

        return $this;
    }

    /**
     * Get the value of marca
     */ 
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set the value of marca
     *
     * @return  self
     */ 
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get the value of artiNombre
     *
     * @return  string
     */ 
    public function getArtiNombre()
    {
        return $this->artiNombre;
    }

    /**
     * Set the value of artiNombre
     *
     * @param  string  $artiNombre
     *
     * @return  self
     */ 
    public function setArtiNombre(string $artiNombre)
    {
        $this->artiNombre = $artiNombre;

        return $this;
    }

    /**
     * Get the value of artiDescri
     *
     * @return  string|null
     */ 
    public function getArtiDescri()
    {
        return $this->artiDescri;
    }

    /**
     * Set the value of artiDescri
     *
     * @param  string|null  $artiDescri
     *
     * @return  self
     */ 
    public function setArtiDescri($artiDescri)
    {
        $this->artiDescri = $artiDescri;

        return $this;
    }

    /**
     * Get the value of unidId
     *
     * @return  int|null
     */ 
    public function getUnidId()
    {
        return $this->unidId;
    }

    /**
     * Set the value of unidId
     *
     * @param  int|null  $unidId
     *
     * @return  self
     */ 
    public function setUnidId($unidId)
    {
        $this->unidId = $unidId;

        return $this;
    }

    /**
     * Get the value of artiStock
     *
     * @return  float|null
     */ 
    public function getArtiStock()
    {
        return $this->artiStock;
    }

    /**
     * Set the value of artiStock
     *
     * @param  float|null  $artiStock
     *
     * @return  self
     */ 
    public function setArtiStock($artiStock)
    {
        $this->artiStock = $artiStock;

        return $this;
    }

    /**
     * Get the value of artiStockmin
     *
     * @return  float|null
     */ 
    public function getArtiStockmin()
    {
        return $this->artiStockmin;
    }

    /**
     * Set the value of artiStockmin
     *
     * @param  float|null  $artiStockmin
     *
     * @return  self
     */ 
    public function setArtiStockmin($artiStockmin)
    {
        $this->artiStockmin = $artiStockmin;

        return $this;
    }

    /**
     * Get the value of artiPvp
     *
     * @return  float
     */ 
    public function getArtiPvp()
    {
        return $this->artiPvp;
    }

    /**
     * Set the value of artiPvp
     *
     * @param  float  $artiPvp
     *
     * @return  self
     */ 
    public function setArtiPvp(float $artiPvp)
    {
        $this->artiPvp = $artiPvp;

        return $this;
    }

    /**
     * Get the value of artiFechapvp
     *
     * @return  \DateTime|null
     */ 
    public function getArtiFechapvp()
    {
        return $this->artiFechapvp;
    }

    /**
     * Set the value of artiFechapvp
     *
     * @param  \DateTime|null  $artiFechapvp
     *
     * @return  self
     */ 
    public function setArtiFechapvp($artiFechapvp)
    {
        $this->artiFechapvp = $artiFechapvp;

        return $this;
    }

    /**
     * Get the value of artiPreciocosto
     *
     * @return  float|null
     */ 
    public function getArtiPreciocosto()
    {
        return $this->artiPreciocosto;
    }

    /**
     * Set the value of artiPreciocosto
     *
     * @param  float|null  $artiPreciocosto
     *
     * @return  self
     */ 
    public function setArtiPreciocosto($artiPreciocosto)
    {
        $this->artiPreciocosto = $artiPreciocosto;

        return $this;
    }

    /**
     * Get the value of artiFechapcosto
     *
     * @return  \DateTime|null
     */ 
    public function getArtiFechapcosto()
    {
        return $this->artiFechapcosto;
    }

    /**
     * Set the value of artiFechapcosto
     *
     * @param  \DateTime|null  $artiFechapcosto
     *
     * @return  self
     */ 
    public function setArtiFechapcosto($artiFechapcosto)
    {
        $this->artiFechapcosto = $artiFechapcosto;

        return $this;
    }

    /**
     * Get the value of proveeId
     *
     * @return  int|null
     */ 
    public function getProveeId()
    {
        return $this->proveeId;
    }

    /**
     * Set the value of proveeId
     *
     * @param  int|null  $proveeId
     *
     * @return  self
     */ 
    public function setProveeId($proveeId)
    {
        $this->proveeId = $proveeId;

        return $this;
    }

    /**
     * Get the value of aivaId
     *
     * @return  int|null
     */ 
    public function getAivaId()
    {
        return $this->aivaId;
    }

    /**
     * Set the value of aivaId
     *
     * @param  int|null  $aivaId
     *
     * @return  self
     */ 
    public function setAivaId($aivaId)
    {
        $this->aivaId = $aivaId;

        return $this;
    }

    /**
     * Get the value of monedaId
     *
     * @return  int|null
     */ 
    public function getMonedaId()
    {
        return $this->monedaId;
    }

    /**
     * Set the value of monedaId
     *
     * @param  int|null  $monedaId
     *
     * @return  self
     */ 
    public function setMonedaId($monedaId)
    {
        $this->monedaId = $monedaId;

        return $this;
    }

    /**
     * Get the value of artiUbicacion
     *
     * @return  string|null
     */ 
    public function getArtiUbicacion()
    {
        return $this->artiUbicacion;
    }

    /**
     * Set the value of artiUbicacion
     *
     * @param  string|null  $artiUbicacion
     *
     * @return  self
     */ 
    public function setArtiUbicacion($artiUbicacion)
    {
        $this->artiUbicacion = $artiUbicacion;

        return $this;
    }

    /**
     * Get the value of artiObservaciones
     *
     * @return  string|null
     */ 
    public function getArtiObservaciones()
    {
        return $this->artiObservaciones;
    }

    /**
     * Set the value of artiObservaciones
     *
     * @param  string|null  $artiObservaciones
     *
     * @return  self
     */ 
    public function setArtiObservaciones($artiObservaciones)
    {
        $this->artiObservaciones = $artiObservaciones;

        return $this;
    }

    /**
     * Get the value of artiImagenblob
     *
     * @return  string|null
     */ 
    public function getArtiImagenblob()
    {
        return $this->artiImagenblob;
    }

    /**
     * Set the value of artiImagenblob
     *
     * @param  string|null  $artiImagenblob
     *
     * @return  self
     */ 
    public function setArtiImagenblob($artiImagenblob)
    {
        $this->artiImagenblob = $artiImagenblob;

        return $this;
    }

    /**
     * Get the value of artiMargen
     *
     * @return  float|null
     */ 
    public function getArtiMargen()
    {
        return $this->artiMargen;
    }

    /**
     * Set the value of artiMargen
     *
     * @param  float|null  $artiMargen
     *
     * @return  self
     */ 
    public function setArtiMargen($artiMargen)
    {
        $this->artiMargen = $artiMargen;

        return $this;
    }

    public  function __toString()
    {
        return (string) $this->artiCodigoPropio. ' - ' . $this->artiNombre .' - '. $this->marca;
    }
}
