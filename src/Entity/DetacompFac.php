<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DetacompFac
 *
 * @ORM\Table(name="detacomp_fac")
 * @ORM\Entity
 */
class DetacompFac
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Comprobante")
     * @ORM\JoinColumn(name="comp_id", referencedColumnName="id")
     */
    private $comprobante;

    /**
     * @var int
     *
     * @ORM\Column(name="prod_id", type="integer", nullable=false)
     */
    private $prodId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prod_descri", type="string", length=100, nullable=true, options={"default"="''"})
     */
    private $prodDescri = '';

    /**
     * @var float
     *
     * @ORM\Column(name="prod_pvp", type="float", precision=10, scale=0, nullable=false)
     */
    private $prodPvp = '0';

    /**
     * @var float|null
     *
     * @ORM\Column(name="prod_percdto", type="float", precision=10, scale=0, nullable=true)
     */
    private $prodPercdto = '0';

    /**
     * @var float|null
     *
     * @ORM\Column(name="prod_impdto", type="float", precision=10, scale=0, nullable=true)
     */
    private $prodImpdto = '0';

    /**
     * @var float|null
     *
     * @ORM\Column(name="prod_impiva", type="float", precision=10, scale=0, nullable=true)
     */
    private $prodImpiva = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="prod_cant", type="float", precision=10, scale=0, nullable=false)
     */
    private $prodCant = '0';






    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of comprobante
     */ 
    public function getComprobante()
    {
        return $this->comprobante;
    }

    /**
     * Set the value of comprobante
     *
     * @return  self
     */ 
    public function setComprobante($comprobante)
    {
        $this->comprobante = $comprobante;

        return $this;
    }

    /**
     * Get the value of prodId
     *
     * @return  int
     */ 
    public function getProdId()
    {
        return $this->prodId;
    }

    /**
     * Set the value of prodId
     *
     * @param  int  $prodId
     *
     * @return  self
     */ 
    public function setProdId(int $prodId)
    {
        $this->prodId = $prodId;

        return $this;
    }

    /**
     * Get the value of prodDescri
     *
     * @return  string|null
     */ 
    public function getProdDescri()
    {
        return $this->prodDescri;
    }

    /**
     * Set the value of prodDescri
     *
     * @param  string|null  $prodDescri
     *
     * @return  self
     */ 
    public function setProdDescri($prodDescri)
    {
        $this->prodDescri = $prodDescri;

        return $this;
    }

    /**
     * Get the value of prodPvp
     *
     * @return  float
     */ 
    public function getProdPvp()
    {
        return $this->prodPvp;
    }

    /**
     * Set the value of prodPvp
     *
     * @param  float  $prodPvp
     *
     * @return  self
     */ 
    public function setProdPvp(float $prodPvp)
    {
        $this->prodPvp = $prodPvp;

        return $this;
    }

    /**
     * Get the value of prodPercdto
     *
     * @return  float|null
     */ 
    public function getProdPercdto()
    {
        return $this->prodPercdto;
    }

    /**
     * Set the value of prodPercdto
     *
     * @param  float|null  $prodPercdto
     *
     * @return  self
     */ 
    public function setProdPercdto($prodPercdto)
    {
        $this->prodPercdto = $prodPercdto;

        return $this;
    }

    /**
     * Get the value of prodImpdto
     *
     * @return  float|null
     */ 
    public function getProdImpdto()
    {
        return $this->prodImpdto;
    }

    /**
     * Set the value of prodImpdto
     *
     * @param  float|null  $prodImpdto
     *
     * @return  self
     */ 
    public function setProdImpdto($prodImpdto)
    {
        $this->prodImpdto = $prodImpdto;

        return $this;
    }

    /**
     * Get the value of prodImpiva
     *
     * @return  float|null
     */ 
    public function getProdImpiva()
    {
        return $this->prodImpiva;
    }

    /**
     * Set the value of prodImpiva
     *
     * @param  float|null  $prodImpiva
     *
     * @return  self
     */ 
    public function setProdImpiva($prodImpiva)
    {
        $this->prodImpiva = $prodImpiva;

        return $this;
    }

    /**
     * Get the value of prodCant
     *
     * @return  float
     */ 
    public function getProdCant()
    {
        return $this->prodCant;
    }

    /**
     * Set the value of prodCant
     *
     * @param  float  $prodCant
     *
     * @return  self
     */ 
    public function setProdCant(float $prodCant)
    {
        $this->prodCant = $prodCant;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->prodCant . ' - ' . $this->prodDescri . ' - ' . $this->prodPvp;
    }
}
