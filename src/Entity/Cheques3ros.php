<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cheques3ros
 *
 * @ORM\Table(name="cheques3ros")
 * @ORM\Entity
 */
class Cheques3ros
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    // *
    //  * @var int
    //  *
    //  * @ORM\Column(name="bnco_id", type="integer", nullable=false)
     
    // private $banco = '0';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Banco")
     * @ORM\JoinColumn(name="bnco_id", referencedColumnName="id")
     */
    private $banco;

    // *
    //  * @var int|null
    //  *
    //  * @ORM\Column(name="loca_id", type="integer", nullable=true, options={"comment"="plaza"})
     
    // private $localidad = '0';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Localidad")
     * @ORM\JoinColumn(name="loca_id", referencedColumnName="loca_id")
     */
    private $localidad;

   

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Chqestado")
     * @ORM\JoinColumn(name="chqe_id", referencedColumnName="id")
     */
    private $estado ;

    /**
     * @var string
     *
     * @ORM\Column(name="chq3_nume", type="string", length=18, nullable=false)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="chq3_titular", type="string", length=250, nullable=true)
     */
    private $titular = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="chq3_titcuit", type="string", length=60, nullable=true)
     */
    private $cuitDelTitular = '';

    /**
     * @var float
     *
     * @ORM\Column(name="chq3_importe", type="float", precision=10, scale=0, nullable=false)
     */
    private $importe = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="chq3_fecha_cobro", type="string", nullable=false)
     */
    private $fechaDeCobro;

    /**
     * @var string
     *
     * @ORM\Column(name="chq3_fech_emision", type="string", nullable=true)
     */
    private $fechaDeEmision;

    /**
     * @var string
     *
     * @ORM\Column(name="chq3_fech_recibido", type="string", nullable=false)
     */
    private $fechaDeRecibido;

   

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Chq3destino")
     * @ORM\JoinColumn(name="chqd_id", referencedColumnName="id")
     */
    private $destino;

    /**
     * @var string
     *
     * @ORM\Column(name="chq3_fech_salida", type="string", nullable=true)
     */
    private $fechaDeSalida;

    /**
     * @var int
     *
     * @ORM\Column(name="pers_id", type="integer", nullable=false, options={"comment"="quien me dio el cheque"})
     */
    private $persona = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_descri", type="string", length=200, nullable=true, options={"comment"="Quien me dio el cheque, por si no esta en persona"})
     */
    private $personaDescripcion = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="chq3_obse", type="string", length=250, nullable=true)
     */
    private $observaciones = '';





    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBanco()
    {
        return $this->banco;
    }

    /**
     * @param mixed $banco
     *
     * @return self
     */
    public function setBanco($banco)
    {
        $this->banco = $banco;

        return $this;
    }

    

    

    /**
     * @return mixed
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * @param mixed $localidad
     *
     * @return self
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;

        return $this;
    }

   

    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param string $numero
     *
     * @return self
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitular()
    {
        return $this->titular;
    }

    /**
     * @param string|null $titular
     *
     * @return self
     */
    public function setTitular($titular)
    {
        $this->titular = $titular;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCuitDelTitular()
    {
        return $this->cuitDelTitular;
    }

    /**
     * @param string|null $cuitDelTitular
     *
     * @return self
     */
    public function setCuitDelTitular($cuitDelTitular)
    {
        $this->cuitDelTitular = $cuitDelTitular;

        return $this;
    }

    /**
     * @return float
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * @param float $importe
     *
     * @return self
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * @return string
     */
    public function getFechaDeCobro()
    {
        return $this->fechaDeCobro;
    }

    /**
     * @param string $fechaDeCobro
     *
     * @return self
     */
    public function setFechaDeCobro($fechaDeCobro)
    {
        $this->fechaDeCobro = $fechaDeCobro;

        return $this;
    }

    /**
     * @return string
     */
    public function getFechaDeEmision()
    {
        return $this->fechaDeEmision;
    }

    /**
     * @param string $fechaDeEmision
     *
     * @return self
     */
    public function setFechaDeEmision($fechaDeEmision)
    {
        $this->fechaDeEmision = $fechaDeEmision;

        return $this;
    }

    /**
     * @return string
     */
    public function getFechaDeRecibido()
    {
        return $this->fechaDeRecibido;
    }

    /**
     * @param string $fechaDeRecibido
     *
     * @return self
     */
    public function setFechaDeRecibido($fechaDeRecibido)
    {
        $this->fechaDeRecibido = $fechaDeRecibido;

        return $this;
    }

   
    /**
     * @return string
     */
    public function getFechaDeSalida()
    {
        return $this->fechaDeSalida;
    }

    /**
     * @param string $fechaDeSalida
     *
     * @return self
     */
    public function setFechaDeSalida($fechaDeSalida)
    {
        $this->fechaDeSalida = $fechaDeSalida;

        return $this;
    }

    /**
     * @return int
     */
    public function getPersona()
    {
        return $this->persona;
    }

    /**
     * @param int $persona
     *
     * @return self
     */
    public function setPersona($persona)
    {
        $this->persona = $persona;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPersonaDescripcion()
    {
        return $this->personaDescripcion;
    }

    /**
     * @param string|null $personaDescripcion
     *
     * @return self
     */
    public function setPersonaDescripcion($personaDescripcion)
    {
        $this->personaDescripcion = $personaDescripcion;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * @param string|null $observaciones
     *
     * @return self
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

     public function __toString()
    {
        return (string) $this->banco . ' - ' . $this->numero. ' - ' . $this->titular. ' - ' . $this->fecha;
    }




    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    

    /**
     * @return mixed
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * @param mixed $destino
     *
     * @return self
     */
    public function setDestino($destino)
    {
        $this->destino = $destino;

        return $this;
    }
}
