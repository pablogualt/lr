<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Persona
 *
 * @ORM\Table(name="persona")
 * @ORM\Entity
 */
class Persona
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tdoc")
     * @ORM\JoinColumn(name="tdoc_id", referencedColumnName="id")
     */
    private $tipoDoc = 25;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_cuit", type="text", length=11, nullable=false)
     */
    private $cuit = '00000000000';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Civa")
     * @ORM\JoinColumn(name="civa_id", referencedColumnName="id")
     */
    private $condicionDeIva = 6;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_docume", type="text", length=8, nullable=false)
     */
    private $documento = '000000000';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="pers_pjurid", type="boolean", nullable=false)
     */
    private $pjuridica = false;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pers_nroleg", type="integer", nullable=true)
     */
    private $nrolegajo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_direcc", type="text", length=150, nullable=false)
     */
    private $direccion = '';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Localidad")
     * @ORM\JoinColumn(name="loca_id", referencedColumnName="loca_id")
     */
    private $localidad;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_nombre", type="text", length=60, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_nomb_fanta", type="text", length=100, nullable=false)
     */
    private $nombFantasia = 'sin';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_ibrutos", type="text", length=50, nullable=true)
     */
    private $ibrutos = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_telfax", type="text", length=50, nullable=true)
     */
    private $telFax = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_celular", type="text", length=50, nullable=true)
     */
    private $celular = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_email", type="text", length=100, nullable=true)
     */
    private $email = 'sin';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_contacto", type="text", length=50, nullable=true)
     */
    private $contacto = 'sin';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_celcontacto", type="text", length=50, nullable=true)
     */
    private $celContacto = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_observacion", type="text", length=255, nullable=true)
     */
    private $observacion = 'sin';

    /**
     * @var int
     *
     * @ORM\Column(name="zona_id", type="integer", nullable=false)
     */
    private $zona = 0;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="pers_gremio", type="boolean", nullable=true, options={"comment"="profesional  o particular para diferenciar listas "})
     */
    private $gremio = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="pers_ctacte", type="boolean", nullable=true, options={"comment"="si es ctacte se habilita en la venta "})
     */
    private $ctacte = '0';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }






    /**
     * @return mixed
     */
    public function getTipoDoc()
    {
        return $this->tipoDoc;
    }

    /**
     * @return null|string
     */
    public function getCuit()
    {
        return $this->cuit;
    }

    /**
     * @return mixed
     */
    public function getCondicionDeIva()
    {
        return $this->condicionDeIva;
    }

    /**
     * @return null|string
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * @return bool|null
     */
    public function getPjuridica()
    {
        return $this->pjuridica;
    }

    /**
     * @return int|null
     */
    public function getNrolegajo()
    {
        return $this->nrolegajo;
    }

    /**
     * @return null|string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @return mixed
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getNombFantasia()
    {
        return $this->nombFantasia;
    }

    /**
     * @return null|string
     */
    public function getIbrutos()
    {
        return $this->ibrutos;
    }

    /**
     * @return null|string
     */
    public function getTelFax()
    {
        return $this->telFax;
    }

    /**
     * @return null|string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return null|string
     */
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * @return null|string
     */
    public function getCelContacto()
    {
        return $this->celContacto;
    }

    /**
     * @return null|string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * @return mixed
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * @return bool|null
     */
    public function getGremio()
    {
        return $this->gremio;
    }

    /**
     * @return bool|null
     */
    public function getCtacte()
    {
        return $this->ctacte;
    }

    /**
     * @param mixed $tipoDoc
     */
    public function setTipoDoc($tipoDoc)
    {
        $this->tipoDoc = $tipoDoc;
    }

    /**
     * @param null|string $cuit
     */
    public function setCuit($cuit)
    {
        $this->cuit = $cuit;
    }

    /**
     * @param mixed $condicionDeIva
     */
    public function setCondicionDeIva($condicionDeIva)
    {
        $this->condicionDeIva = $condicionDeIva;
    }

    /**
     * @param null|string $documento
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    /**
     * @param bool|null $pjuridica
     */
    public function setPjuridica($pjuridica)
    {
        $this->pjuridica = $pjuridica;
    }

    /**
     * @param int|null $nrolegajo
     */
    public function setNrolegajo($nrolegajo)
    {
        $this->nrolegajo = $nrolegajo;
    }

    /**
     * @param null|string $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @param mixed $localidad
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @param null|string $nombFantasia
     */
    public function setNombFantasia($nombFantasia)
    {
        $this->nombFantasia = $nombFantasia;
    }

    /**
     * @param null|string $ibrutos
     */
    public function setIbrutos($ibrutos)
    {
        $this->ibrutos = $ibrutos;
    }

    /**
     * @param null|string $telFax
     */
    public function setTelFax($telFax)
    {
        $this->telFax = $telFax;
    }

    /**
     * @param null|string $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @param null|string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param null|string $contacto
     */
    public function setContacto($contacto)
    {
        $this->contacto = $contacto;
    }

    /**
     * @param null|string $celContacto
     */
    public function setCelContacto($celContacto)
    {
        $this->celContacto = $celContacto;
    }

    /**
     * @param null|string $observacion
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    }

    /**
     * @param mixed $zona
     */
    public function setZona($zona)
    {
        $this->zona = $zona;
    }

    /**
     * @param bool|null $gremio
     */
    public function setGremio($gremio)
    {
        $this->gremio = $gremio;
    }

    /**
     * @param bool|null $ctacte
     */
    public function setCtacte($ctacte)
    {
        $this->ctacte = $ctacte;
    }



    public function __toString()
    {
        return (string) $this->nrolegajo . ' - ' . $this->nombre . ' - ' . $this->direccion;
    }

}
