<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proveedor2
 *
 * @ORM\Table(name="proveedor_full")
 * @ORM\Entity(readOnly=true)
 */
class Proveedor2
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

  /**
     * @var string|null
     *
     * @ORM\Column(name="tdoc_desc", type="text", length=11, nullable=false)
     */
    private $tipoDoc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_cuit", type="text", length=11, nullable=false)
     */
    private $cuit = '00000000000';

    /**
     * @var string|null
     *
     * @ORM\Column(name="civa_desc", type="text", length=11, nullable=false)
     */
    private $condicionDeIva ;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_docume", type="text", length=8, nullable=false)
     */
    private $documento = '000000000';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="pers_pjurid", type="boolean", nullable=false)
     */
    private $pjuridica = false;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pers_nroleg", type="integer", nullable=true)
     */
    private $nrolegajo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_direcc", type="text", length=150, nullable=false)
     */
    private $direccion = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="loca_nombre", type="text", length=60, nullable=false)
     */
    private $localidad;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_nombre", type="text", length=60, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_nomb_fanta", type="text", length=100, nullable=false)
     */
    private $nombFantasia = 'sin';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_ibrutos", type="text", length=50, nullable=true)
     */
    private $ibrutos = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_telfax", type="text", length=50, nullable=true)
     */
    private $telFax = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_celular", type="text", length=50, nullable=true)
     */
    private $celular = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_email", type="text", length=100, nullable=true)
     */
    private $email = 'sin';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_contacto", type="text", length=50, nullable=true)
     */
    private $contacto = 'sin';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_celcontacto", type="text", length=50, nullable=true)
     */
    private $celContacto = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pers_observacion", type="text", length=255, nullable=true)
     */
    private $observacion = 'sin';

    /**
     * @var int
     *
     * @ORM\Column(name="zona_id", type="integer", nullable=false)
     */
    private $zona = 0;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="pers_gremio", type="boolean", nullable=true, options={"comment"="profesional  o particular para diferenciar listas "})
     */
    private $gremio = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="pers_ctacte", type="boolean", nullable=true, options={"comment"="si es ctacte se habilita en la venta "})
     */
    private $ctacte = '0';

    



    public function __toString()
    {
        return (string) $this->nrolegajo . ' - ' . $this->nombre . ' - ' . $this->direccion;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTipoDoc()
    {
        return $this->tipoDoc;
    }

    /**
     * @return string|null
     */
    public function getCuit()
    {
        return $this->cuit;
    }

    /**
     * @return string|null
     */
    public function getCondicionDeIva()
    {
        return $this->condicionDeIva;
    }

    /**
     * @return string|null
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * @return bool|null
     */
    public function isPjuridica()
    {
        return $this->pjuridica;
    }

    /**
     * @return int|null
     */
    public function getNrolegajo()
    {
        return $this->nrolegajo;
    }

    /**
     * @return string|null
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @return string|null
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string|null
     */
    public function getNombFantasia()
    {
        return $this->nombFantasia;
    }

    /**
     * @return string|null
     */
    public function getIbrutos()
    {
        return $this->ibrutos;
    }

    /**
     * @return string|null
     */
    public function getTelFax()
    {
        return $this->telFax;
    }

    /**
     * @return string|null
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * @return string|null
     */
    public function getCelContacto()
    {
        return $this->celContacto;
    }

    /**
     * @return string|null
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * @return int
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * @return bool|null
     */
    public function isGremio()
    {
        return $this->gremio;
    }

    /**
     * @return bool|null
     */
    public function isCtacte()
    {
        return $this->ctacte;
    }
}
