<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Comprobante
 *
 * @ORM\Table(name="comprobante")
 * @ORM\Entity
 */
class Comprobante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comp_fecha", type="string", nullable=false)
     */
    private $compFecha;

    // /**
    //  * @var int
    //  *
    //  * @ORM\Column(name="pers_id", type="integer", nullable=false, options={"comment"="Cliente"})
    //  */
    // private $persId;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Persona")
     * @ORM\JoinColumn(name="pers_id", referencedColumnName="id")
     */
    private $cliente;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="comp_ctacte", type="integer", nullable=true, options={"comment"="0 significa Contado"})
     */
    private $compCtacte = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="tcmp_id", type="integer", nullable=false)
     */
    private $tcmpId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_pdventa", type="string", length=5, nullable=true, options={"default"="'0'","comment"="ojo pvta 00000 no existe - ojo"})
     */
    private $compPdventa = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="comp_numero", type="string", length=8, nullable=false, options={"default"="'0'"})
     */
    private $compNumero = '0';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_subtot1", type="float", precision=8, scale=2, nullable=true, options={"default"="0.00","comment"="total antes del descuento general"})
     */
    private $compSubtot1 = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_desc_pctje", type="float", precision=8, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $compDescPctje = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_descuento", type="float", precision=8, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $compDescuento = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_impuest", type="float", precision=8, scale=2, nullable=true, options={"default"="0.00","comment"="total de Otros impuestos"})
     */
    private $compImpuest = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_subtot2", type="float", precision=8, scale=2, nullable=true, options={"default"="0.00","comment"="NETO GRAVADO"})
     */
    private $compSubtot2 = '0.00';

    /**
     * @var int|null
     *
     * @ORM\Column(name="aiva_id", type="integer", nullable=true)
     */
    private $aivaId = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="comp_totiva", type="float", precision=8, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $compTotiva = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="comp_total", type="float", precision=10, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $compTotal = '0.00';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_concepto", type="string", length=250, nullable=true, options={"default"="''","comment"="corresponde a CONCEPTO de PAGO"})
     */
    private $compConcepto = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_observaciones", type="string", length=200, nullable=true, options={"default"="''"})
     */
    private $compObservaciones = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="mone_id", type="integer", nullable=true, options={"default"="1","comment"="peso = 1"})
     */
    private $moneId = '1';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_cambio", type="float", precision=8, scale=2, nullable=true, options={"default"="0.00","comment"="TIPO DE CAMBIO VALOR AL DIA DE LA FACTURA EJ DOLAR PRECIO "})
     */
    private $compCambio = '1.00';

    /**
     * @var int|null
     *
     * @ORM\Column(name="logi_id", type="integer", nullable=true, options={"default"="1"})
     */
    private $logiId = '1';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="comp_apro", type="boolean", nullable=true, options={"comment"="defecto 0 NO apropiado"})
     */
    private $compApro = '0';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_saldo", type="float", precision=8, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $compSaldo = '0.00';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_fecha_venc", type="string", nullable=true, options={"default"="NULL","comment"="defecto copiar fecha factura"})
     */
    private $compFechaVenc = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="mepa_id", type="integer", nullable=true, options={"default"="1","comment"="para fact contado sin recibo"})
     */
    private $mepaId = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_cae", type="string", length=30, nullable=true, options={"default"="''"})
     */
    private $compCae = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_fvcae", type="string", length=30, nullable=true, options={"default"="''"})
     */
    private $compFvcae = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_aliciva", type="text", length=255, nullable=true, options={"default"="NULL","comment"="segun codigo AFIP ej 0005"})
     */
    private $compAliciva = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_cbarra", type="text", length=255, nullable=true, options={"default"="NULL","comment"="segun RG AFIP"})
     */
    private $compCbarra = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_timestamp", type="string", options={"default"=""})
     */
    protected $compTimestamp = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_PeriodoDesde", type="string", nullable=true)
     */
    private $compPeriododesde = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_PeriodoHasta", type="string", nullable=true)
     */
    private $compPeriodohasta = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comp_PeriodoVtoPago", type="string", nullable=true)
     */
    private $compPeriodovtopago = null;

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_nogravado", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00","comment"="total NO gravado (TotConc)"})
     */
    private $compNogravado = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_exento", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00","comment"="total Exento (OpEx)"})
     */
    private $compExento = '0.00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="comp_percIVA", type="float", precision=11, scale=2, nullable=true, options={"default"="0.00","comment"="Percepciones de IVA (redundante con Otributos)"})
     */
    private $compPerciva = '0.00';



    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }



    /**
     * Get the value of cliente
     */ 
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set the value of cliente
     *
     * @return  self
     */ 
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get the value of compCtacte
     *
     * @return  bool|null
     */ 
    public function getCompCtacte()
    {
        return $this->compCtacte;
    }

    /**
     * Set the value of compCtacte
     *
     * @param  bool|null  $compCtacte
     *
     * @return  self
     */ 
    public function setCompCtacte($compCtacte)
    {
        $this->compCtacte = $compCtacte;

        return $this;
    }

    /**
     * Get the value of tcmpId
     *
     * @return  int
     */ 
    public function getTcmpId()
    {
        return $this->tcmpId;
    }

    /**
     * Set the value of tcmpId
     *
     * @param  int  $tcmpId
     *
     * @return  self
     */ 
    public function setTcmpId(int $tcmpId)
    {
        $this->tcmpId = $tcmpId;

        return $this;
    }

    /**
     * Get the value of compPdventa
     *
     * @return  string|null
     */ 
    public function getCompPdventa()
    {
        return $this->compPdventa;
    }

    /**
     * Set the value of compPdventa
     *
     * @param  string|null  $compPdventa
     *
     * @return  self
     */ 
    public function setCompPdventa($compPdventa)
    {
        $this->compPdventa = $compPdventa;

        return $this;
    }

    /**
     * Get the value of compNumero
     *
     * @return  string
     */ 
    public function getCompNumero()
    {
        return $this->compNumero;
    }

    /**
     * Set the value of compNumero
     *
     * @param  string  $compNumero
     *
     * @return  self
     */ 
    public function setCompNumero(string $compNumero)
    {
        $this->compNumero = $compNumero;

        return $this;
    }

    /**
     * Get the value of compSubtot1
     *
     * @return  float|null
     */ 
    public function getCompSubtot1()
    {
        return $this->compSubtot1;
    }

    /**
     * Set the value of compSubtot1
     *
     * @param  float|null  $compSubtot1
     *
     * @return  self
     */ 
    public function setCompSubtot1($compSubtot1)
    {
        $this->compSubtot1 = $compSubtot1;

        return $this;
    }

    /**
     * Get the value of compDescPctje
     *
     * @return  float|null
     */ 
    public function getCompDescPctje()
    {
        return $this->compDescPctje;
    }

    /**
     * Set the value of compDescPctje
     *
     * @param  float|null  $compDescPctje
     *
     * @return  self
     */ 
    public function setCompDescPctje($compDescPctje)
    {
        $this->compDescPctje = $compDescPctje;

        return $this;
    }

    /**
     * Get the value of compDescuento
     *
     * @return  float|null
     */ 
    public function getCompDescuento()
    {
        return $this->compDescuento;
    }

    /**
     * Set the value of compDescuento
     *
     * @param  float|null  $compDescuento
     *
     * @return  self
     */ 
    public function setCompDescuento($compDescuento)
    {
        $this->compDescuento = $compDescuento;

        return $this;
    }

    /**
     * Get the value of compImpuest
     *
     * @return  float|null
     */ 
    public function getCompImpuest()
    {
        return $this->compImpuest;
    }

    /**
     * Set the value of compImpuest
     *
     * @param  float|null  $compImpuest
     *
     * @return  self
     */ 
    public function setCompImpuest($compImpuest)
    {
        $this->compImpuest = $compImpuest;

        return $this;
    }

    /**
     * Get the value of compSubtot2
     *
     * @return  float|null
     */ 
    public function getCompSubtot2()
    {
        return $this->compSubtot2;
    }

    /**
     * Set the value of compSubtot2
     *
     * @param  float|null  $compSubtot2
     *
     * @return  self
     */ 
    public function setCompSubtot2($compSubtot2)
    {
        $this->compSubtot2 = $compSubtot2;

        return $this;
    }

    /**
     * Get the value of aivaId
     *
     * @return  int|null
     */ 
    public function getAivaId()
    {
        return $this->aivaId;
    }

    /**
     * Set the value of aivaId
     *
     * @param  int|null  $aivaId
     *
     * @return  self
     */ 
    public function setAivaId($aivaId)
    {
        $this->aivaId = $aivaId;

        return $this;
    }

    /**
     * Get the value of compTotiva
     *
     * @return  float
     */ 
    public function getCompTotiva()
    {
        return $this->compTotiva;
    }

    /**
     * Set the value of compTotiva
     *
     * @param  float  $compTotiva
     *
     * @return  self
     */ 
    public function setCompTotiva(float $compTotiva)
    {
        $this->compTotiva = $compTotiva;

        return $this;
    }

    /**
     * Get the value of compTotal
     *
     * @return  float
     */ 
    public function getCompTotal()
    {
        return $this->compTotal;
    }

    /**
     * Set the value of compTotal
     *
     * @param  float  $compTotal
     *
     * @return  self
     */ 
    public function setCompTotal(float $compTotal)
    {
        $this->compTotal = $compTotal;

        return $this;
    }

    /**
     * Get the value of compConcepto
     *
     * @return  string|null
     */ 
    public function getCompConcepto()
    {
        return $this->compConcepto;
    }

    /**
     * Set the value of compConcepto
     *
     * @param  string|null  $compConcepto
     *
     * @return  self
     */ 
    public function setCompConcepto($compConcepto)
    {
        $this->compConcepto = $compConcepto;

        return $this;
    }

    /**
     * Get the value of compObservaciones
     *
     * @return  string|null
     */ 
    public function getCompObservaciones()
    {
        return $this->compObservaciones;
    }

    /**
     * Set the value of compObservaciones
     *
     * @param  string|null  $compObservaciones
     *
     * @return  self
     */ 
    public function setCompObservaciones($compObservaciones)
    {
        $this->compObservaciones = $compObservaciones;

        return $this;
    }

    /**
     * Get the value of moneId
     *
     * @return  int|null
     */ 
    public function getMoneId()
    {
        return $this->moneId;
    }

    /**
     * Set the value of moneId
     *
     * @param  int|null  $moneId
     *
     * @return  self
     */ 
    public function setMoneId($moneId)
    {
        $this->moneId = $moneId;

        return $this;
    }

    /**
     * Get the value of compCambio
     *
     * @return  float|null
     */ 
    public function getCompCambio()
    {
        return $this->compCambio;
    }

    /**
     * Set the value of compCambio
     *
     * @param  float|null  $compCambio
     *
     * @return  self
     */ 
    public function setCompCambio($compCambio)
    {
        $this->compCambio = $compCambio;

        return $this;
    }

    /**
     * Get the value of logiId
     *
     * @return  int|null
     */ 
    public function getLogiId()
    {
        return $this->logiId;
    }

    /**
     * Set the value of logiId
     *
     * @param  int|null  $logiId
     *
     * @return  self
     */ 
    public function setLogiId($logiId)
    {
        $this->logiId = $logiId;

        return $this;
    }

    /**
     * Get the value of compApro
     *
     * @return  bool|null
     */ 
    public function getCompApro()
    {
        return $this->compApro;
    }

    /**
     * Set the value of compApro
     *
     * @param  bool|null  $compApro
     *
     * @return  self
     */ 
    public function setCompApro($compApro)
    {
        $this->compApro = $compApro;

        return $this;
    }

    /**
     * Get the value of compSaldo
     *
     * @return  float|null
     */ 
    public function getCompSaldo()
    {
        return $this->compSaldo;
    }

    /**
     * Set the value of compSaldo
     *
     * @param  float|null  $compSaldo
     *
     * @return  self
     */ 
    public function setCompSaldo($compSaldo)
    {
        $this->compSaldo = $compSaldo;

        return $this;
    }

    

    /**
     * Get the value of mepaId
     *
     * @return  int|null
     */ 
    public function getMepaId()
    {
        return $this->mepaId;
    }

    /**
     * Set the value of mepaId
     *
     * @param  int|null  $mepaId
     *
     * @return  self
     */ 
    public function setMepaId($mepaId)
    {
        $this->mepaId = $mepaId;

        return $this;
    }

    /**
     * Get the value of compCae
     *
     * @return  string|null
     */ 
    public function getCompCae()
    {
        return $this->compCae;
    }

    /**
     * Set the value of compCae
     *
     * @param  string|null  $compCae
     *
     * @return  self
     */ 
    public function setCompCae($compCae)
    {
        $this->compCae = $compCae;

        return $this;
    }

    /**
     * Get the value of compFvcae
     *
     * @return  string|null
     */ 
    public function getCompFvcae()
    {
        return $this->compFvcae;
    }

    /**
     * Set the value of compFvcae
     *
     * @param  string|null  $compFvcae
     *
     * @return  self
     */ 
    public function setCompFvcae($compFvcae)
    {
        $this->compFvcae = $compFvcae;

        return $this;
    }

    /**
     * Get the value of compAliciva
     *
     * @return  string|null
     */ 
    public function getCompAliciva()
    {
        return $this->compAliciva;
    }

    /**
     * Set the value of compAliciva
     *
     * @param  string|null  $compAliciva
     *
     * @return  self
     */ 
    public function setCompAliciva($compAliciva)
    {
        $this->compAliciva = $compAliciva;

        return $this;
    }

    /**
     * Get the value of compCbarra
     *
     * @return  string|null
     */ 
    public function getCompCbarra()
    {
        return $this->compCbarra;
    }

    /**
     * Set the value of compCbarra
     *
     * @param  string|null  $compCbarra
     *
     * @return  self
     */ 
    public function setCompCbarra($compCbarra)
    {
        $this->compCbarra = $compCbarra;

        return $this;
    }


    /**
     * Get the value of compPeriododesde
     *
     * @param  string 
     */ 
    public function getCompPeriododesde()
    {
        return $this->compPeriododesde;
    }

    /**
     * Set the value of compPeriododesde
     *
     * @param  string  $compPeriododesde
     *
     * @return  self
     */ 
    public function setCompPeriododesde( string $compPeriododesde)
    {
        $this->compPeriododesde = $compPeriododesde;

        return $this;
    }

    /**
     * Get the value of compPeriodohasta
     *
     * @param  string 
     */ 
    public function getCompPeriodohasta()
    {
        return $this->compPeriodohasta;
    }

    /**
     * Set the value of compPeriodohasta
     *
     * @param  string   $compPeriodohasta
     *
     * @return  self
     */ 
    public function setCompPeriodohasta(string $compPeriodohasta)
    {
        $this->compPeriodohasta = $compPeriodohasta;

        return $this;
    }


    /**
     * Get the value of compFecha
     *
     * @return  string
     */
    public function getCompFecha()
    {
        return $this->compFecha;
    }

    /**
     * Set the value of compFecha
     *
     * @param  string  $compFecha
     *
     * @return  self
     */
    public function setCompFecha(string $compFecha)
    {
        $this->compFecha = $compFecha;

        return $this;
    }

    /**
     * Get the value of compPeriodovtopago
     *
     * @param  string 
     */ 
    public function getCompPeriodovtopago()
    {
        return $this->compPeriodovtopago;
    }

    /**
     * Set the value of compPeriodovtopago
     *
     * @param  string   $compPeriodovtopago
     *
     * @return  self
     */ 
    public function setCompPeriodovtopago( string $compPeriodovtopago)
    {
        $this->compPeriodovtopago = $compPeriodovtopago;

        return $this;
    }

    /**
     * Get the value of compNogravado
     *
     * @return  float|null
     */ 
    public function getCompNogravado()
    {
        return $this->compNogravado;
    }

    /**
     * Set the value of compNogravado
     *
     * @param  float|null  $compNogravado
     *
     * @return  self
     */ 
    public function setCompNogravado($compNogravado)
    {
        $this->compNogravado = $compNogravado;

        return $this;
    }

    /**
     * Get the value of compExento
     *
     * @return  float|null
     */ 
    public function getCompExento()
    {
        return $this->compExento;
    }

    /**
     * Set the value of compExento
     *
     * @param  float|null  $compExento
     *
     * @return  self
     */ 
    public function setCompExento($compExento)
    {
        $this->compExento = $compExento;

        return $this;
    }

    /**
     * Get the value of compPerciva
     *
     * @return  float|null
     */ 
    public function getCompPerciva()
    {
        return $this->compPerciva;
    }

    /**
     * Set the value of compPerciva
     *
     * @param  float|null  $compPerciva
     *
     * @return  self
     */ 
    public function setCompPerciva($compPerciva)
    {
        $this->compPerciva = $compPerciva;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->compFecha . ' - ' . $this->cliente . ' - ' . $this->compTotal;
    }

    // public function __construct()
    // {
    //     $this->compTimestamp = new \DateTime();
    // }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * /**
     * @param mixed $compTimestamp
     */
    public function setCompTimestamp($compTimestamp)
    {
        $this->compTimestamp = $compTimestamp;
    }


    /**
     * Get the value of compFechaVenc
     *
     * @return  string|null
     */ 
    public function getCompFechaVenc()
    {
        return $this->compFechaVenc;
    }

    /**
     * Set the value of compFechaVenc
     *
     * @param  string|null  $compFechaVenc
     *
     * @return  self
     */ 
    public function setCompFechaVenc($compFechaVenc)
    {
        $this->compFechaVenc = $compFechaVenc;

        return $this;
    }
}
