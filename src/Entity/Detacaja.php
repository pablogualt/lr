<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Detacaja
 *
 * @ORM\Table(name="detacaja")
 * @ORM\Entity
 */
class Detacaja
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

  
     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cajadiaria")
     * @ORM\JoinColumn(name="caja_id", referencedColumnName="id")
     */
    private $caja;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Mediodepago")
     * @ORM\JoinColumn(name="mepa_id", referencedColumnName="id")
     */
    private $medioDePago;


    /**
     * @var int|null
     *
     * @ORM\Column(name="asoc_id", type="integer", nullable=true, options={"comment"="id asociado si hubiera, ej chqe_id o transf_id"})
     */
    private $asocId = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="decaj_importe", type="float", precision=10, scale=0, nullable=false)
     */
    private $importe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="decaj_obse", type="string", length=200, nullable=true)
     */
    private $observaciones = '';

    public function __toString()
    {
        return (string) $this->importe;

    }





    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCaja()
    {
        return $this->caja;
    }

    /**
     * @param mixed $caja
     *
     * @return self
     */
    public function setCaja($caja)
    {
        $this->caja = $caja;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMedioDePago()
    {
        return $this->medioDePago;
    }

    /**
     * @param mixed $medioDePago
     *
     * @return self
     */
    public function setMedioDePago($medioDePago)
    {
        $this->medioDePago = $medioDePago;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAsocId()
    {
        return $this->asocId;
    }

    /**
     * @param int|null $asocId
     *
     * @return self
     */
    public function setAsocId($asocId)
    {
        $this->asocId = $asocId;

        return $this;
    }

    /**
     * @return float
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * @param float $importe
     *
     * @return self
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * @param string|null $observaciones
     *
     * @return self
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }
}
