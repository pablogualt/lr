<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cajadiaria
 *
 * @ORM\Table(name="cajadiaria")
 * @ORM\Entity
 */
class Cajadiaria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="comp_id", type="integer", nullable=true, options={"comment"="comp asociado ej. factura, orden de pago, etc"})
     */
    private $comprobante = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="caja_desc", type="string", length=200, nullable=true)
     */
    private $descripcion = '';

   

    /**
     * @var string
     *
     * @ORM\Column(name="caja_fecha", type="string", nullable=false)
     */
    private $fecha;

    /**
     * @var float
     *
     * @ORM\Column(name="caja_debe", type="float", precision=11, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $debe = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="caja_haber", type="float", precision=11, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $haber = '0.00';

      //  /**
      //   * @ORM\ManyToOne(targetEntity="App\Entity\Persona")
      //   * @ORM\JoinColumn(name="pers_id", referencedColumnName="id")
      //   */
    //private $cliente;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pers_id", type="integer", nullable=true, options={"comment"="persona asociada al movimiento, ej al que le di el vale"})
     */
     private $persona = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="caja_pdventa", type="string", length=4, nullable=true, options={"comment"="sin uso"})
     */
    private $puntoDeVenta = '02';

   


     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tipocomp")
     * @ORM\JoinColumn(name="tipocomp_id", referencedColumnName="id")
     */
    private $tipoComprobante;

    /**
     * @var int|null
     *
     * @ORM\Column(name="logi_id", type="integer", nullable=true)
     */
    protected $user = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="caja_timestamp", type="string", options={"default"=""}))
     */
     protected $compTimestamp = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="caja_observa", type="string", length=200)
     */
    private $observaciones = '';




     public function __toString()
    {
        return (string) $this->fecha . ' D- ' . $this->debe. ' H- ' . $this->haber;
    }



    /**
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

   

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getComprobante()
    {
        return $this->comprobante;
    }

    /**
     * @param int|null $comprobante
     *
     * @return self
     */
    public function setComprobante($comprobante)
    {
        $this->comprobante = $comprobante;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     *
     * @return self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

   

    /**
     * @return float
     */
    public function getDebe()
    {
        return $this->debe;
    }

    /**
     * @param float $debe
     *
     * @return self
     */
    public function setDebe($debe)
    {
        $this->debe = $debe;

        return $this;
    }

    /**
     * @return float
     */
    public function getHaber()
    {
        return $this->haber;
    }

    /**
     * @param float $haber
     *
     * @return self
     */
    public function setHaber($haber)
    {
        $this->haber = $haber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     *
     * @return self
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPersona()
    {
        return $this->persona;
    }

    /**
     * @param int|null $persona
     *
     * @return self
     */
    public function setPersona($persona)
    {
        $this->persona = $persona;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPuntoDeVenta()
    {
        return $this->puntoDeVenta;
    }

    /**
     * @param string|null $puntoDeVenta
     *
     * @return self
     */
    public function setPuntoDeVenta($puntoDeVenta)
    {
        $this->puntoDeVenta = $puntoDeVenta;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTipoComprobante()
    {
        return $this->tipoComprobante;
    }

    /**
     * @param int|null $tipoComprobante
     *
     * @return self
     */
    public function setTipoComprobante($tipoComprobante)
    {
        $this->tipoComprobante = $tipoComprobante;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int|null $user
     *
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCompTimestamp()
    {
        return $this->compTimestamp;
    }

    /**
     * @param string|null $compTimestamp
     *
     * @return self
     */
    public function setCompTimestamp($compTimestamp)
    {
        $this->compTimestamp = $compTimestamp;

        return $this;
    }

   

    /**
     * @return string
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param string $fecha
     *
     * @return self
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @param string|null $observaciones
     *
     * @return self
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }
}
