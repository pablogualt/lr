<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Chqestado
 *
 * @ORM\Table(name="chqestado")
 * @ORM\Entity
 */
class Chqestado
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="chqe_descri", type="string", length=100, nullable=false)
     */
    private $descripcion;


     public function __toString()
    {
        return (string) $this->descripcion;
    }



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     *
     * @return self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }
}
